#!/usr/bin/env conda-execute


# conda execute
# env:
#  - conda ==4.1.12
#  - conda-build ==2.0.6
#  - conda-build-all ==1.1.0
#  - git
#  - conda-gitenv ==0.2.2
#  - pip
# channels:
#  - conda-forge
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e


PROG=$(basename ${0} .sh)


usage() {
    echo -e "\nusage: ${PROG} [-h] PLATFORM\n"
    echo -e "Build conda recipe platform specific RPMs and populate the associated platform RPM repository.\n"
    echo    "positional arguments:"
    echo -e "  PLATFORM      the target platform [${PLATFORMS}]\n"
    echo    "optional arguments:"
    echo -e "  -h, --help    show this help message and exit\n"

    exit 0
}


oops() {
    local EMSG="${1}"

    echo -e "\n${PROG}: ${EMSG}\n"

    exit 1
}


# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

AGNOSTIC_REPO="ssh://git@exxgitrepo:7999/sss/environments.git"

# Define platform specific associative arrays.
declare -A REPO
REPO[cray]="ssh://git@exxgitrepo:7999/sss/crayhpc-environments.git"
REPO[rhel6]="ssh://git@exxgitrepo:7999/sss/scidesktop-environments.git"
PLATFORMS=$(echo "${!REPO[@]}" | sed "s/ /|/g")

declare -A ALIAS
ALIAS[cray]="cray"
ALIAS[rhel6]="scidesktop"

declare -A MODULE_PREFIX
MODULE_PREFIX[cray]="/critical/scitools/modulefiles"
MODULE_PREFIX[rhel6]="/usr/share/Modules/modulefiles/scitools"

# Sanity check the command line arguments.
[ ${#} -ne 1 ] && usage
[ "${1}" == "-h" -o "${1}" == "--help" ] && usage
PLATFORM=$(echo "${1}" | tr [A-Z] [a-z])
if [ -z "${REPO[${PLATFORM}]}" ]
then
    EMSG="Require a valid PLATFORM argument, got \"${1}\", expected [${PLATFORMS}]."
    oops "${EMSG}"
fi

# In order for this git to use Met Office self-signed certificates, we need to enable them.
# We should re-build certifi with the Met Office certs to avoid this.
# Worse, some systems (looking at the els machines, have extremely old ca-certificate RPMs that
# pre-date the REPO) have extremely old cacert files, so we have copied the certificates
# into this repo from /etc/pki/tls/cert.pem.
cp ${SCRIPT_DIR}/cert.pem ${PREFIX}/ssl/cacert.pem

# Configuration file
CONFIG_YAML=${DATADIR}/conda_rpms_${PLATFORM}.config

# RPM state file
STATE_YAML=${DATADIR}/conda_rpms_${PLATFORM}.state
STATE_FNAME=rpm_state.sh
STATE_SCRIPT=${SCRIPT_DIR}/${STATE_FNAME}

# Locked-down parent directory of output spec files
SPECS_LOCATION=${DATADIR}/rpmbuildroot_${PLATFORM}
mkdir -p ${SPECS_LOCATION}
chmod 0700 ${SPECS_LOCATION}

# RPM build directory
mkdir -p ${SPECS_LOCATION}/SPECS

# Directory to check for built RPMs
BUILT_RPMS_DIR=${SPECS_LOCATION}/RPMS/x86_64

# Destination for RPM repository on the remote host
REMOTE_RPMS_DIR=/var/www/html/${PLATFORM}/RPMS
REMOTE_HOSTNAME=exvscitoolsrepoprd
REMOTE_USER=avd

# Platform specific configuration base directories
PLATFORM_DIR=${SCRIPT_DIR}/../../${ALIAS[${PLATFORM}]}
PLATFORM_SPECS=${PLATFORM_DIR}/SPECS

# RPM package prefix
PACKAGE=SciTools

# Configure conda-rpms to our taste.
cat <<-EOF1 >${CONFIG_YAML}
rpm:
    prefix: ${PACKAGE}

install:
    prefix: /opt/scitools

module:
    prefix: ${MODULE_PREFIX[${PLATFORM}]}
    file: ${PLATFORM_DIR}/modulefile
    default: ${SCRIPT_DIR}/modulefile.default
EOF1

# Default agnostic config yaml file
AGNOSTIC_CONFIG_YAML=${DATADIR}/conda_rpms_${PLATFORM}_agnostic.config

cat <<-EOF2 >${AGNOSTIC_CONFIG_YAML}
rpm:
    prefix: ${PACKAGE}

install:
    prefix: /opt/scitools

module:
    prefix: ${MODULE_PREFIX[${PLATFORM}]}
    file: ${PLATFORM_DIR}/modulefile.agnostic
    default: ${SCRIPT_DIR}/modulefile.default
EOF2

# Determine the environment label RPMs that have already been registered
# with the remote yum RPM repository, and generate an environment label
# state yaml file for use by conda-rpms.
# The state yaml file allows conda-rpms to determine whether an environment
# label for a branch in the environment definition repository is new, or has
# been changed to reference a different environment tag since the last
# execution of conda-rpms. As a result, conda-rpms will now be able to only
# build new or changed environment label RPMs, and all their associated
# dependency RPMs, rather than build RPMs for every label, in every branch,
# all the time.
REMOTE_TMPDIR=$(ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} "mktemp -d" 2>/dev/null)
REMOTE_STATE_SCRIPT=${REMOTE_TMPDIR}/${STATE_FNAME}
scp -o BatchMode=yes ${STATE_SCRIPT} ${REMOTE_USER}@${REMOTE_HOSTNAME}:${REMOTE_STATE_SCRIPT} >/dev/null 2>&1
ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} "${REMOTE_STATE_SCRIPT} ${PACKAGE} ${REMOTE_RPMS_DIR}" >${STATE_YAML} 2>/dev/null
ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} "rm -rf ${REMOTE_TMPDIR}" >/dev/null 2>&1

# Capture the build RPM config and state for logging purposes.
echo "${CONFIG_YAML}:"
cat ${CONFIG_YAML}
echo -e "\n${AGNOSTIC_CONFIG_YAML}:"
cat ${AGNOSTIC_CONFIG_YAML}
echo -e "\n${STATE_YAML}:"
cat ${STATE_YAML}

# Install other dependencies that aren't packaged with conda yet.
pip install https://github.com/SciTools-incubator/conda-rpms/archive/v1.5.2.tar.gz

# Disable ssl verification in order to access Artifactory
conda config --set ssl_verify False

# Set artifactory authentication details.
ARTIFACTORY_DIR='/home/h06/avd/.artifactory'
API_USER='avd.linux'
API_KEY=$(head -n1 ${ARTIFACTORY_DIR}/api_key)

# Build the RPM specifications for artifactory-cloud agnostic environments.
python -m conda_rpms.build_rpm_structure \
            ${AGNOSTIC_REPO} \
            ${SPECS_LOCATION} \
            -c ${AGNOSTIC_CONFIG_YAML} \
            -s ${STATE_YAML} \
            -u ${API_USER} \
            -k ${API_KEY} \
            --env_labels experimental/current default/next

# Copy any static RPM specifications into the SPECS directory for building.
if [ -d "${PLATFORM_SPECS}" ]
then
    cp ${PLATFORM_SPECS}/*.spec ${SPECS_LOCATION}/SPECS
fi

# Build the RPMs.
python -m conda_rpms.build ${SPECS_LOCATION} ${BUILT_RPMS_DIR}

# Sign the built RPMs and set appropriate permissions.
for FNAME in ${BUILT_RPMS_DIR}/*.rpm
do
    # Ensure user and group read-write permissions and other read-only permissions.
    chmod -R 0664 ${FNAME}

    # Sign the RPMs with the AVD GPG key.
    # Remove carriage returns from the output.
    ${SCRIPT_DIR}/sign_rpm.exp ${FNAME} | sed 's/\r//'
done

# Copy the built RPMs into the remote RPM repository.
CHANGES=$(rsync -icrlpoD --ignore-existing ${BUILT_RPMS_DIR} ${REMOTE_USER}@${REMOTE_HOSTNAME}:${REMOTE_RPMS_DIR})

if [ -n "${CHANGES}" ]
then
    # Create/update the RPM repository.
    ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} createrepo -v --update --database ${REMOTE_RPMS_DIR}

    # Make the RPM repository browseable.
    TITLE=$(echo "${PLATFORM}" | tr [a-z] [A-Z])
    ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} repoview -t ${TITLE} ${REMOTE_RPMS_DIR}

    # Ensure correct group ownership.
    ssh -o BatchMode=yes ${REMOTE_USER}@${REMOTE_HOSTNAME} chown -R :avdteam ${REMOTE_RPMS_DIR}
    echo "Updated"
else
    echo "Unchanged"
fi
