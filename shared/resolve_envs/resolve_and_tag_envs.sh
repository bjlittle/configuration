#!/usr/bin/env bash

# conda execute
# env:
#  - conda 4.3.*
#  - conda-build <3.18.3
#  - conda-build-all
#  - conda-gitenv
#  - git
#  - python
# channels:
#  - conda-forge
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e


PROG=$(basename ${0} .sh)


oops() {
  local EMSG="${1}"

  echo -e "\n${PROG}: ERROR - ${EMSG}\n"

  exit 1
}


################################################################################

[ -z "${ENV_REPO_PREFIX}" ] && oops 'Unset environment variable "${ENV_REPO_PREFIX}".'
[ "${ENV_REPO_PREFIX}" != "agnostic" ] && oops "Only supporting \"agnostic\" environments, got \"${ENV_REPO_PREFIX}\"."
ENV_REPO=ssh://git@exxgitrepo:7999/sss/environments.git

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

BASE_DIR="${SCRATCH}/${PROG}"
[ -d "${BASE_DIR}" ]  && rm -rf ${BASE_DIR}
mkdir -p ${BASE_DIR}

# Set artifactory authentication details.
ARTIFACTORY_DIR='/home/h06/avd/.artifactory'
API_USER='avd.linux'
API_KEY=$(head -n1 ${ARTIFACTORY_DIR}/api_key)

# Remove pip as a dependency of python - we only want to install pip if it is
# part of the environment definition (or a package explicitly depends upon it).
# Disable ssl verification in order to work with Artifactory.
export CONDARC="${SCRATCH}/conda/${PROG}_condarc"
mkdir -p $(dirname ${CONDARC})
cat <<EOF >${CONDARC}

add_pip_as_python_dependency: False
ssl_verify: False

EOF

################################################################################
echo -e "\n${PROG}: Resolve artifactory-cloud environments...\n"

# Resolve artifactory-cloud "development" environments.
time conda gitenv resolve ${ENV_REPO} --api_key=${API_KEY} --api_user=${API_USER} --envs experimental
time conda gitenv resolve ${ENV_REPO} --api_key=${API_KEY} --api_user=${API_USER} --envs default

# Resolve artifactory-cloud "pre-production" environments.
time conda gitenv resolve ${ENV_REPO} --api_key=${API_KEY} --api_user=${API_USER} --envs preproduction

# Resolve artifactory-cloud "production" environments.
time conda gitenv resolve ${ENV_REPO} --api_key=${API_KEY} --api_user=${API_USER} --envs production

################################################################################
echo -e "\n${PROG}: Tag environments...\n"

# Tag the environments.
AUTOTAG_FILE="${BASE_DIR}/autotag.txt"
conda gitenv autotag ${ENV_REPO} > ${AUTOTAG_FILE}
[ -s "${AUTOTAG_FILE}" ] && cat ${AUTOTAG_FILE}

################################################################################
echo -e "\n${PROG}: Cache environment packages...\n"

if [ -s "${AUTOTAG_FILE}" ]
then

  # Clone the environments git repository locally.
  GIT_BASE_DIR="${BASE_DIR}/repo"
  [ -d "${GIT_BASE_DIR}" ] && rm -rf ${GIT_BASE_DIR}
  mkdir -p ${GIT_BASE_DIR}
  git clone ${ENV_REPO} ${GIT_BASE_DIR}

  while IFS= read -r LINE
  do

    # Parse the autotag line, which has the form "Pushing tag <TAG>"
    TAG_NAME=$(echo "${LINE}" | cut -d' ' -f3)
    echo -e "\nCaching \"${TAG_NAME}\" manifest packages..."
    cd ${GIT_BASE_DIR}
    # Checkout the specified tagged environment.
    git checkout ${TAG_NAME} >/dev/null 2>&1
    # Force artifactory to cache the manifest packages.
    # Assumes that we are only tagging environments from artifactory-cloud.
    ENV_MANIFEST="${GIT_BASE_DIR}/env.manifest"
    python ${SCRIPT_DIR}/cache_packages.py ${ENV_MANIFEST} --api_user=${API_USER} --api_key=${API_KEY}

  done < "${AUTOTAG_FILE}"

  echo "Updated"

else

  echo "Unchanged"

fi
