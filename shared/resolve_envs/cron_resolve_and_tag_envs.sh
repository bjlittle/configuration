#!/usr/bin/env bash

# Purpose:
#   Cron job to resolve and tag the environments defined in a repository for
#   the specificied ${ENV_REPO_PREFIX}.
# Details:
#   ${ENV_REPO_PREFIX} is set in the crontab. Currently, this is either
#   'scidesktop' or 'crayhpc' or 'agnostic'.
#   ${ENV_REPO_PREFIX} must match the name of the environment definition
#   repository.

#==============================================================================

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)

if [ -z "${ENV_REPO_PREFIX}" ]
then
    echo
    echo 'Resolve: Unset environment variable ${ENV_REPO_PREFIX}.'
    echo
    exit 1
fi

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOG_FILE=${LOG_DIR}/${ENV_REPO_PREFIX}_resolve_envs.$(date +%Y-%m-%d-%H%M%S).log

# Define a temporary path for conda environments. conda-execute uses this
# as a basis of its own temporary environment cache.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda/resolve_envs/envs

# Run the main command + capture a success flag.
EXECUTE=~avd/live/conda-execute/bin/conda-execute
unbuffer ${EXECUTE} -vf ${SCRIPT_DIR}/resolve_and_tag_envs.sh >${LOG_FILE} 2>&1

RETURNCODE=${?}
UNCHANGED=$(tail -5 ${LOG_FILE} | grep -ic "^unchanged$")

if [ ${RETURNCODE} -ne 0 ]
then
    echo
    echo "Resolve: Failed for environment ${ENV_REPO_PREFIX} on ${HOSTNAME} [exit=${RETURNCODE}] - check log file \"${LOG_FILE}\"."
    echo
    cat ${LOG_FILE}
elif [ ${UNCHANGED} -eq 0 ]
then
    echo
    echo "Resolve: Changed for environment ${ENV_REPO_PREFIX} on ${HOSTNAME} - check log file \"${LOG_FILE}\"."
    echo
    cat ${LOG_FILE}
fi
