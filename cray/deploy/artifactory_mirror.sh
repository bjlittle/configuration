#!/usr/bin/env bash

# conda execute
# env:
#  - conda 4.3.*
#  - conda-build <3.18.3
#  - conda-build-all
#  - conda-gitenv
#  - git
#  - python
# channels:
#  - conda-forge
# run_with: bash

################################################################################

trap 'echo "Aborted!"; exit 1' ERR
set -e


PROG=$(basename ${0} .sh)


usage() {
  cat <<EOF

Usage: ${PROG} [-d <deploy-dir>] [-k <api-key>] [-m <mirror-dir>] [-u <api-user>] [-h] repo

Create a local mirror of the conda channels and conda packages required to provision
the Scientific Software Stack repository environments from Artifactory.

-h, -help,       --help          Show this help message and exit.
-d, -deploy_dir, --deploy_dir    Set the environment deployment directory.
                                 Defaults to "${DEFAULT_DEPLOY_DIR}".
-m, -mirror_dir, --mirror_dir    Set the mirror base directory that will contain the
                                 local conda channels that mirror Artifactory.
                                 Defaults to "${DEFAULT_MIRROR_DIR}".
-k, -api_key,    --api_key       Set the Artifactory API key.
-u, -api_user,   --api_user      Set the Artifactory API user.
repo                             Git repository directory/URL (mandatory).

EOF

  exit 0
}


oops() {
  local EMSG="${1}"

  echo -e "\n${PROG}: ERROR - ${EMSG}\n"

  exit 1
}


################################################################################

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Configure the defaults.
BASE_DIR="${SCRATCH}/${PROG}"
DEFAULT_DEPLOY_DIR="${BASE_DIR}/deploy"
DEFAULT_MIRROR_DIR="/project/avd/live/conda/${PROG}"
DEPLOY_DIR=${DEFAULT_DEPLOY_DIR}
MIRROR_DIR=${DEFAULT_MIRROR_DIR}

# Parse the optional command line arguments.
# Reference: https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash
OPTIONS=$(getopt -l "help,deploy_dir:,mirror_dir:,api_key:,api_user:" -o "hd::m::k:u:" -a -- "${@}")

eval set -- "${OPTIONS}"

while true
do
  case ${1} in
  -h|--help)
    usage
    ;;
  -d|--deploy_dir)
    shift
    export DEPLOY_DIR="${1}"
    ;;
  -m|--mirror_dir)
    shift
    export MIRROR_DIR="${1}"
    ;;
  -k|--api_key)
    shift
    export ART_API_KEY="${1}"
    ;;
  -u|--api_user)
    shift
    export ART_API_USER="${1}"
    ;;
  --)
    shift
    break
    ;;
  esac

shift
done

# Parse the mandatory command line arguments.
[ "${#}" -ne 1 ] && usage
GIT_ENV_REPO="${1}"

################################################################################
echo -e "\n${PROG}: Create artifactory-cloud mirror...\n"

echo -e "\tART_API_KEY=<$([ -n "${ART_API_KEY}" ] && echo "********")>"
echo -e "\tART_API_USER=<${ART_API_USER}>"
echo -e "\tDEPLOY_DIR=<${DEPLOY_DIR}>"
echo -e "\tGIT_ENV_REPO=<${GIT_ENV_REPO}>"
echo -e "\tMIRROR_DIR=<${MIRROR_DIR}>"
echo -e "\tMIRROR_SKIP=<${MIRROR_SKIP}>"

if [ -n "${MIRROR_SKIP}" ]
then
  echo -e "\n${PROG}: Skipping mirroring process, on request.\n"
  exit 0
fi

################################################################################
echo -e "\n${PROG}: Tidy previous deployment directory...\n"

echo -e "Started at $(date)...\n"

# Tidy any previous deployment directory.
[ -d "${DEPLOY_DIR}" ] && time rm -rf ${DEPLOY_DIR}
mkdir -p ${DEPLOY_DIR}

# Ensure that the mirror directory exists.
[ ! -d "${MIRROR_DIR}" ] && mkdir ${MIRROR_DIR}

################################################################################
echo -e "\n${PROG}: Deploy artifactory-cloud environments...\n"

if [[ -n "${ART_API_USER}" && -n "${ART_API_KEY}" ]]
then

    # Deploy artifactory-cloud development environments.
    echo -e "Started at $(date)...\n"
    time conda gitenv deploy ${GIT_ENV_REPO} ${DEPLOY_DIR} --api_user=${ART_API_USER} --api_key=${ART_API_KEY} --env_labels experimental/current
    # time conda gitenv deploy ${GIT_ENV_REPO} ${DEPLOY_DIR} --api_user=${ART_API_USER} --api_key=${ART_API_KEY} --env_labels default/previous
    # time conda gitenv deploy ${GIT_ENV_REPO} ${DEPLOY_DIR} --api_user=${ART_API_USER} --api_key=${ART_API_KEY} --env_labels default/current
    time conda gitenv deploy ${GIT_ENV_REPO} ${DEPLOY_DIR} --api_user=${ART_API_USER} --api_key=${ART_API_KEY} --env_labels default/next

fi

################################################################################
echo -e "\n${PROG}: Populate local artifactory mirror channels...\n"

echo -e "Started at $(date)...\n"

PKG_CACHE_DIR="${DEPLOY_DIR}/.pkg_cache"
PKG_CACHE_URLS="${PKG_CACHE_DIR}/urls.txt"

[ ! -d "${PKG_CACHE_DIR}" ] && oops "Missing deployment package cache directory, \"${PKG_CACHE_DIR}\"."
[ ! -f "${PKG_CACHE_URLS}" ] && oops "Missing deployment package cache file, \"${PKG_CACHE_URLS}\"."

echo -e "\tPKG_CACHE_DIR=<${PKG_CACHE_DIR}>"
echo -e "\tPKG_CACHE_URLS=<${PKG_CACHE_URLS}>\n"

time while IFS= read -r PKG_URL
do

  PKG="$(basename ${PKG_URL})"
  ARCH="$(basename $(dirname ${PKG_URL}))"
  CHANNEL_DIR="${MIRROR_DIR}/${ARCH}"
  PKG_FILE="${PKG_CACHE_DIR}/${PKG}"
  MIRROR_FILE="${CHANNEL_DIR}/${PKG}"
  mkdir -p ${CHANNEL_DIR}
  echo -e "Mirroring <${ARCH}> package <${PKG}>"
  echo -e "\t${PKG_FILE} -> ${MIRROR_FILE}"
  if [ -f "${PKG_FILE}" ]
  then
    # Copy the package iff it doesn't already exist in the target mirror channel.
    [ ! -f "${MIRROR_FILE}" ] && cp ${PKG_FILE} ${MIRROR_FILE}
  else
    [ ! -f "${MIRROR_FILE}" ] && oops "Failed to mirror missing package cache file, \"${PKG_FILE}\"."
    echo -e "\tWARNING: Package cache file \"${PKG_FILE}\" is missing, but available in mirror channel!"
  fi

done < "${PKG_CACHE_URLS}"

################################################################################
echo -e "\n${PROG}: Index local artifactory mirror channels...\n"

echo -e "Started at $(date)...\n"

echo -e "Indexing mirror channel <${MIRROR_DIR}>"
conda index ${MIRROR_DIR}
