#!/usr/bin/env bash

################################################################################
#
# Purpose:
#   Cron job that mirrors the relevant artifactory-cloud channels and packages
#   locally, and then runs a PBS job to deploy development environments on the
#   'CRAY' platform.
#
################################################################################

trap 'echo "Aborted!"; exit 1' ERR
set -e

PROG=$(basename ${0} .sh)

# Check the availability of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Set up LOGFILE for output of this script.
LOGDIR="${SCRATCH}/logs"
mkdir -p ${LOGDIR}

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd)

# Source BitBucket repository.
ENV_REPO="ssh://git@exxgitrepo:7999/sss/environments.git"

# CRAY remote hosts.
REMOTES=(xcel00 xcfl00)

################################################################################
#
# mirror the artifactory cloud conda channels locally
#

ARTIFACTORY_DIR="/home/h06/avd/.artifactory"
API_USER="avd.linux"
API_KEY=$(head -n1 ${ARTIFACTORY_DIR}/api_key)
LOGFILE="${LOGDIR}/artifactory_mirror.$(date +%Y-%m-%d-%H%M%S).log"

# Define a temporary path for conda environments.
# This is used by conda-execute as a basis of its own temporary environment cache.
export CONDA_ENVS_PATH="${LOCALTEMP}/conda/${PROG}/envs"

# Tidy any previous conda environment.
[ -d "${CONDA_ENVS_PATH}" ] && rm -rf ${CONDA_ENVS_PATH}

# Run the main command + capture a success flag.
EXECUTE=~avd/live/conda-execute/bin/conda-execute
unbuffer ${EXECUTE} -vf ${SCRIPT_DIR}/artifactory_mirror.sh ${ENV_REPO} --api_key=${API_KEY} --api_user=${API_USER} >${LOGFILE} 2>&1

RC=${?}
if [ ${RC} -ne 0 ]
then
    echo
    echo "${PROG}: Failed to mirror artifactory channels - see log file \"${LOGFILE}\"."
    echo
    cat ${LOGFILE}
    exit 1
fi

################################################################################
#
# mirror the bitbucket cloud "environments" git repository on the CRAY
# remote hosts
#

MIRROR_USER="avd"

for TARGET_REMOTE in "${REMOTES[@]}"
do
    LOGFILE="${LOGDIR}/bitbucket_mirror_${TARGET_REMOTE}.$(date +%Y-%m-%d-%H%M%S).log"
    SOURCE_DIR="${SCRATCH}/bitbucket_mirror_${TARGET_REMOTE}"
    COMMAND='echo ${DATADIR}'
    TARGET_DIR=$(ssh -o BatchMode=yes ${MIRROR_USER}@${TARGET_REMOTE} ${COMMAND})

    ${SCRIPT_DIR}/bitbucket_mirror.sh ${ENV_REPO} --source_dir=${SOURCE_DIR} --target_dir=${TARGET_DIR} --mirror_host=${TARGET_REMOTE} --mirror_user=${MIRROR_USER} >${LOGFILE} 2>&1

    RC=${?}
    if [ ${RC} -ne 0 ]
    then
        echo
        echo "${PROG}: Failed to mirror bitbucket repository - see log file \"${LOGFILE}\"."
        echo
        exit 1
    fi
done

################################################################################
#
# schedule PBS jobs on the CRAY to deploy development environments
#

for TARGET_REMOTE in "${REMOTES[@]}"
do
    ${SCRIPT_DIR}/cray_pbs.sh ${TARGET_REMOTE}

    RC=${?}
    if [ ${RC} -ne 0 ]
    then
        echo
        echo "${PROG}: Failed to launch CRAY PBS deploy job on ${TARGET_REMOTE}"
        echo
        exit 1
    fi
done
