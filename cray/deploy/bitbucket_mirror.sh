#!/usr/bin/env bash

################################################################################

trap 'echo "Aborted!"; exit 1' ERR
set -e


PROG=$(basename ${0} .sh)


usage() {
  cat <<EOF

Usage: ${PROG} [-m <remote-mirror-hostname>] [-s <local-source-dir>] [-t <remote-target-dir>] [-h] repo

Create a remote mirror of the environments BitBucket git repository required
to provision the Scientific Software Stack.

repo                             URL of BitBucket repository to mirror.

-h, -help, --help                Show this help message and exit.
-m, -mirror_host, --mirror_host  Provide the remote mirror hostname.
-s, -source_dir, --source_dir    Provide the local mirror source directory name.
-t, -target_dir, --target_dir    Provide the remote mirror target base directory name.
-u, -mirror_user, --mirror_user  Provide the user for the remote mirror host.

EOF

  exit 0
}


oops() {
  local EMSG="${1}"

  echo -e "\n${PROG}: ERROR - ${EMSG}\n"

  exit 1
}


################################################################################

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Parse the optional command line arguments.
# Reference: https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash
OPTIONS=$(getopt -l "help,mirror_host:,mirror_user:,source_dir:,target_dir:" -o "hm:u:s:t:" -a -- "${@}")

eval set -- "${OPTIONS}"

while true
do
  case ${1} in
  -h|--help)
    usage
    ;;
  -m|--mirror_host)
    shift
    export MIRROR_HOSTNAME="${1}"
    ;;
  -u|--mirror_user)
    shift
    export MIRROR_USER="${1}"
    ;;
  -s|--source_dir)
    shift
    export MIRROR_SOURCE_DIR="${1}"
    ;;
  -k|--target_dir)
    shift
    export MIRROR_TARGET_BASE_DIR="${1}"
    ;;
  --)
    shift
    break
    ;;
  esac

shift
done

# Parse the mandatory command line arguments.
[ "${#}" -ne 1 ] && usage
BITBUCKET_ENV_REPO="${1}"

################################################################################
echo -e "\n${PROG}: Create bitbucket mirror...\n"

echo -e "\tBITBUCKET_ENV_REPO=<${BITBUCKET_ENV_REPO}>"
echo -e "\tMIRROR_HOSTNAME=<${MIRROR_HOSTNAME}>"
echo -e "\tMIRROR_SKIP=<${MIRROR_SKIP}>"
echo -e "\tMIRROR_SOURCE_DIR=<${MIRROR_SOURCE_DIR}>"
echo -e "\tMIRROR_TARGET_BASE_DIR=<${MIRROR_TARGET_BASE_DIR}>"
echo -e "\tMIRROR_USER=<${MIRROR_USER}>"

if [ -n "${MIRROR_SKIP}" ]
then
  echo -e "\n${PROG}: Skipping mirroring process, on request.\n"
  exit 0
fi

################################################################################
echo -e "\n${PROG}: Clone to local mirror directory...\n"

echo -e "Started at $(date)...\n"

# Tidy any previous local mirror directory.
[ -d "${MIRROR_SOURCE_DIR}" ] && rm -rf ${MIRROR_SOURCE_DIR}
mkdir -p ${MIRROR_SOURCE_DIR}

# Clone the BitBucket repository.
git clone --quiet ${BITBUCKET_ENV_REPO} ${MIRROR_SOURCE_DIR}

################################################################################
echo -e "\n${PROG}: Clone to remote mirror directory...\n"

echo -e "Started at $(date)...\n"

BASENAME=$(basename ${MIRROR_SOURCE_DIR})
MIRROR_TARGET_DIR=${MIRROR_TARGET_BASE_DIR}/${BASENAME}

# Tidy any previous remote mirror directory.
COMMAND="if [ -d ${MIRROR_TARGET_DIR} ]; then rm -rf ${MIRROR_TARGET_DIR}; fi"
ssh -o BatchMode=yes ${MIRROR_USER}@${MIRROR_HOSTNAME} ${COMMAND}

# Create the remote mirror directory.
COMMAND="mkdir -p ${MIRROR_TARGET_DIR}"
ssh -o BatchMode=yes ${MIRROR_USER}@${MIRROR_HOSTNAME} ${COMMAND}

# Copy the git clone to the remote mirror directory.
scp -r -o BatchMode=yes ${MIRROR_SOURCE_DIR} ${MIRROR_USER}@${MIRROR_HOSTNAME}:${MIRROR_TARGET_BASE_DIR}
