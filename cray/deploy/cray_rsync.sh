#!/usr/bin/env bash


#trap 'echo "Aborted!"; exit 1' ERR
#set -e


PROG=$(basename ${0} .sh)


usage() {
    echo -e "\nusage: ${PROG} [-h] PULL-HOST PUSH-HOST\n"
    echo -e "Transfer deployment payload from <PULL-HOST> to <PUSH-HOST> via rsync.\n"
    echo    "positional arguments:"
    echo    "  PULL-HOST     the source deployment platform"
    echo -e "  PUSH-HOST     the target deployment platform\n"
    echo    "optional arguments:"
    echo -e "  -h, --help    show this help message and exit\n"

    exit 0
}


oops() {
    local EMSG="${1}"

    echo -e "\n${PROG}: ${EMSG}\n"

    exit 1
}


rsync_pull() {
    local PULL_HOST="${1}"
    local BASE_DIR="${2}"
    local DIRS="${3}"
    local RSYNC_DIR="${4}"
    local RSYNC_FLAGS
    local DIR
    local PREFIX
    local COMMAND
    local READLINK_DIR
    local LINK
    local TARGET_DIR


    RSYNC_FLAGS="-icrlpoDH"
    RSYNC_DIR=${RSYNC_DIR}/$(basename ${BASE_DIR})

    echo -e "\n<- pull ----------------------------------------------------------------------->"
    echo "$(date)"
    echo "PULL_HOST = <${PULL_HOST}>"
    echo "BASE_DIR = <${BASE_DIR}>"
    echo -e "RSYNC_DIR = <${RSYNC_DIR}>\n"

    # Ensure the rsync directory exists.
    mkdir -p ${RSYNC_DIR}

    # Pull each of the specified remote directories from the pull host,
    # where each directory is relative to the specified pull base directory.
    for DIR in ${DIRS}
    do
        # Capture the directory prefix, if any.
        PREFIX="$(dirname ${DIR})"
        # Source pull directory is relative to the pull base directory.
        DIR=${BASE_DIR}/${DIR}
        echo "DIR = <${DIR}>"
        # Ensure that the remote source pull directory exists.
        COMMAND="test -e ${DIR}"
        ssh -o BatchMode=yes ${PULL_HOST} ${COMMAND} >/dev/null 2>&1
        if [ ${?} -eq 0 ]
        then
            # Ensure to de-reference any symbolic links.
            COMMAND="readlink -f ${DIR}"
            READLINK_DIR=$(ssh -o BatchMode=yes ${PULL_HOST} ${COMMAND} 2>/dev/null)
            echo "READLINK_DIR = <${READLINK_DIR}>"
            # Test whether the source pull directory is a symbolic link.
            COMMAND="[ -h ${DIR} ] && echo 'true' || echo 'false'"
            LINK=$(ssh -o BatchMode=yes ${PULL_HOST} ${COMMAND} 2>/dev/null)
            echo "LINK = <${LINK}>"
            TARGET_DIR=${RSYNC_DIR}

            if [ "${PREFIX}" != "." ]
            then
                # Make the target directory relative to directory prefix.
                TARGET_DIR=${RSYNC_DIR}/${PREFIX}
            fi

            echo -e "TARGET_DIR = <${TARGET_DIR}>\n"
            # Ensure the target directory exists.
            mkdir -p ${TARGET_DIR}

            # Now perform the rsync from the pull host to the local host.
            unbuffer rsync ${RSYNC_FLAGS} ${PULL_HOST}:${READLINK_DIR} ${TARGET_DIR}

            if [ "${LINK}" == "true" ]
            then
                # Pull the actual link file, as rsync doesn't follow the reference
                # target of the symbolic link automatically.
                unbuffer rsync ${RSYNC_FLAGS} ${PULL_HOST}:${DIR} ${TARGET_DIR}
            fi
        else
            echo -e "\nSkipping - <${DIR}> does not exist on <${PULL_HOST}>\n"
        fi
    done
}


rsync_push() {
    local PUSH_HOST="${1}"
    local PUSH_DIR="${2}"
    local DIRS="${3}"
    local RSYNC_DIR="${4}"
    local RSYNC_FLAGS
    local DIR
    local SOURCE_DIR

    
    RSYNC_FLAGS="-icrlpoDH"

    echo -e "\n<- push ----------------------------------------------------------------------->"
    echo "$(date)"
    echo "PUSH_HOST = <${PUSH_HOST}>"
    echo "PUSH_DIR = <${PUSH_DIR}>"
    echo -e "RSYNC_DIR = <${RSYNC_DIR}>\n"

    # Push each of the specified source directories, which are relative to the
    # rsync directory, to the specified remote push directory.
    for DIR in ${DIRS}
    do
        SOURCE_DIR=${RSYNC_DIR}/${DIR}
        echo -e "SOURCE_DIR = <${SOURCE_DIR}>\n"

        if [ -d "${SOURCE_DIR}" ]
        then
            # Now perform the rsync to the push host from the local host.
            unbuffer rsync ${RSYNC_FLAGS} ${SOURCE_DIR} ${PUSH_HOST}:${PUSH_DIR}
        else
            EMSG="Invalid push source directory, got \"${SOURCE_DIR}\"."
            oops "${EMSG}"
        fi
    done
}


# Sanity check the command line arguments.
[ "${1}" == "-h" -o "${1}" == "--help" ] && usage
[ ${#} -ne 2 ] && usage
PULL_HOST="${1}"
PUSH_HOST="${2}"
HOSTS="${PULL_HOST} ${PUSH_HOST}"
for HOST in ${HOSTS}
do
    ping -c1 ${HOST} >/dev/null 2>&1
    [ ${?} -ne 0 ] && oops "Unknown host, got \"${HOST}\"."
done

PULL_DIR=${DATADIR}/${PROG}_${PULL_HOST}
PUSH_DIR="/critical/scitools"

ENV_DIR="${PUSH_DIR}/environments"
MOD_DIR="${PUSH_DIR}/modulefiles"

#
# rsync pull
#

# Perform an rsync pull of each environment from the remote target host.
ENVS=".pkg_cache experimental/current default/next default/current default/previous"
for ENV in ${ENVS}
do
    rsync_pull "${PULL_HOST}" "${ENV_DIR}" "${ENV}" "${PULL_DIR}"
done

# Perform an rsync pull of each modulefile from the remote target host.
# Ensure not to rsync RPM owned, manually deployed production or legacy modulefiles
# e.g. "production-os40-1", "production-os38-1" and "production".
MODS="experimental-current default-next default-current default-previous .version"
for MOD in ${MODS}
do
    rsync_pull "${PULL_HOST}" "${MOD_DIR}" "${MOD}" "${PULL_DIR}"
done


#
# rsync push
#

DIRS="environments modulefiles"
for DIR in ${DIRS}
do
    rsync_push "${PUSH_HOST}" "${PUSH_DIR}" "${DIR}" "${PULL_DIR}"
done


echo -e "\n<- done ----------------------------------------------------------------------->"
echo "$(date)"
