#PBS -N cray_deploy
#PBS -j oe
#PBS -l walltime=02:00:00
#PBS -l select=1:ncpus=1
#PBS -q shared
#PBS -M ml-avd-support@metoffice.gov.uk
#PBS -m a

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Ensure CRAY_WORKDIR is the same in the calling script (cron_deploy.sh)
CRAY_WORKDIR=${DATADIR}/conda_deploy_${TARGET_HOST}
CRAY_LOGDIR=${CRAY_WORKDIR}/logs
CHANNEL_HOST=els056

AGNOSTIC_ENV_REPO=${DATADIR}/bitbucket_mirror_${TARGET_HOST}
ENV_DIR=/critical/scitools/environments
MOD_DIR=/critical/scitools/modulefiles

# Ensure base directories exists.
mkdir -p ${CRAY_LOGDIR}
mkdir -p ${ENV_DIR}

# The entry-point to conda-execute.
EXECUTE=~avd/live/conda-execute/bin/conda-execute

#=============================================================================
echo -e '\nTASK: Clean up logfiles from previous jobs'

PROBE=$(ssh -o BatchMode=yes ${CHANNEL_HOST} '[ -d ${SCRATCH} ] && echo "true" || echo "false"')
if [ "${PROBE}" == "true" ]
then
    rsync -av ${CRAY_LOGDIR}/ ${CHANNEL_HOST}:/scratch/${USER}/logs/
    rm -f ${CRAY_LOGDIR}/*
fi

#=============================================================================
echo -e '\nTASK: Deploy development environments'

# In order to access the CDN channel via http we must *NOT* use the proxy.
# Without disabling it, http://www-avd/conda/cray_tools will not be reachable.
unset http_proxy
unset https_proxy

# Create a local condarc to ensure that conda will only use the cray_tools
# channel when creating environments.
export CONDARC="${CRAY_WORKDIR}/condarc"
cat <<EOF > ${CONDARC}
channels:
    - http://www-avd/conda/cray_tools

default_channels:
    - http://www-avd/conda/cray_tools

EOF

echo -e "\n${CONDARC}:"
cat ${CONDARC}
echo

DEPLOY_SCRIPT=${CRAY_WORKDIR}/run_deploy.sh
cat <<EOF >${DEPLOY_SCRIPT}
#!/usr/bin/env bash

# conda execute
# env:
#   - conda-gitenv
# channels:
#   - http://www-avd/conda/cray_tools
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e


PROG=$(basename ${0} .sh)

# The local mirror of the artifactory-cloud channels.
MIRROR_URL="http://www-avd/conda/artifactory_mirror"

# Disable ssl verification in order to access Artifactory
conda config --set ssl_verify false

#
# Deploy development environments by using packages from the channels in the artifactory-cloud mirror.
#
echo -e "\n\${PROG}: Deploy experimental/current environment...\n"
echo -e "Started at \$(date)...\n"
time conda gitenv deploy ${AGNOSTIC_ENV_REPO} ${ENV_DIR} --mirror=\${MIRROR_URL} --env_labels experimental/current

echo -e "\n\${PROG}: Deploy default/next environment...\n"
echo -e "Started at \$(date)...\n"
time conda gitenv deploy ${AGNOSTIC_ENV_REPO} ${ENV_DIR} --mirror=\${MIRROR_URL} --env_labels default/next

EOF

chmod u+x ${DEPLOY_SCRIPT}

# Execute the script within a conda-execute (cached) environment.
export CONDA_ENVS_PATH=${CRAY_WORKDIR}/conda/envs
${EXECUTE} -vf ${DEPLOY_SCRIPT}

echo "Done!"

#=============================================================================
echo -e '\nTASK: Tidy the deployed environments'

# Remove the "latest" link created on default and default_legacy by conda-gitenv.
[ -h ${ENV_DIR}/default/latest ] && unlink ${ENV_DIR}/default/latest
[ -f ${MOD_DIR}/default-latest ] && rm -f ${MOD_DIR}/default-latest

[ -h ${ENV_DIR}/experimental/latest ] && unlink ${ENV_DIR}/experimental/latest
[ -f ${MOD_DIR}/experimental-latest ] && rm -f ${MOD_DIR}/experimental-latest

echo "Done!"

#=============================================================================
echo -e '\nTASK: Create modulefiles for deployed environments'

LABEL_LINKS=$(find ${ENV_DIR} -maxdepth 2 -type l)
${EXECUTE} -vf ${CRAY_WORKDIR}/create_modulefile.py -d ${MOD_DIR} ${LABEL_LINKS}

echo "Done!"

#=============================================================================
echo -e '\nTASK: Tidy deployed environments group ownership and permissions'

echo -e "Started at $(date)...\n"
time chgrp -Rf avd ${ENV_DIR} ${MOD_DIR}
echo -e "Started at $(date)...\n"
time chmod -Rf g+w ${ENV_DIR} ${MOD_DIR}

echo "Done!"
