Name:           SciTools-Cray
Version:        9.0
Release:        1
Summary:        The scientific software stack provided for production use on the Cray.

License:        BSD 3
BuildRoot:      %{_tmppath}/SciTools-Cray

Requires: SciTools-env-production-label-os41-1
Requires: SciTools-env-production-label-os42-1
Requires: SciTools-env-production-label-os43-1
Requires: SciTools-env-production-label-os43-2
Requires: SciTools-env-production-label-os44-1
Requires: SciTools-env-production_legacy-label-os42-1
Requires: SciTools-env-production_legacy-label-os43-1
Requires: SciTools-env-production_legacy-label-os43-2


%description

This package installs the software environments that are required for operational use on the Cray.

The package is entirely meta - it has no functionality other than to serve as a single point of installation
 for the Cray and management of obsoleted labels and environments.


%files


%changelog
* Mon Jun 01 2020 Laura Dreyer <laura.dreyer@metoffice.gov.uk> - 9.0-1
- Added production label for os44-1

* Mon Jul 29 2019 Bill Little <bill.little@metoffice.gov.uk> - 8.0-1
- Added production+legacy labels for os43-2

* Thu Jul 18 2019 Bill Little <bill.little@metoffice.gov.uk> - 7.0-1
- Retire production-os40-1

* Wed Jun 12 2019 Bill Little <bill.little@metoffice.gov.uk> - 6.0-1
- Added production+legacy labels for os43-1

* Wed Jan 09 2019 Bill Little <bill.little@metoffice.gov.uk> - 5.0-1
- Added production-os42-1 label

* Wed Oct 17 2018 Bill Little <bill.litte@metoffice.gov.uk> - 4.0-1
- Added production_legacy-os42-1 label
