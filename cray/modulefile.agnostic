#%Module1.0
#
# scitools - scientific software stack for the "{{ env.name }}/{{ env.label }}" environment
#

set ENV_NAME "{{ env.name }}-{{ env.label }}"
set LINK_DIR "{{ env.prefix }}/environments/{{ env.name }}/{{ env.label }}"
set BASE_DIR [ exec readlink -f $LINK_DIR ]
set HELP "Scientific software stack for the '{{ env.name }}/{{ env.label }}' environment."
set EXPERIMENTAL_ENVS {"experimental"}

proc ModulesHelp { } {
    global ENV_NAME
    global LINK_DIR
    global BASE_DIR
    global HELP

    puts stderr $HELP
    puts stderr ""
    puts stderr "This environment is available via:"
    puts stderr "   - the symbolic link $LINK_DIR, or"
    puts stderr "   - the base directory $BASE_DIR"
    puts stderr ""
    puts stderr "Note that, the environment symbolic link points to the environment base"
    puts stderr "directory."
    puts stderr ""
    puts stderr "As new software is deployed to the scientific software stack, this environment"
    puts stderr "symbolic link is automatically updated to point to the appropriate environment"
    puts stderr "base directory."
    puts stderr ""
    puts stderr "For documentation, see http://www-avd/sci/software_stack/"
}

module-whatis $HELP
conflict scitools um_tools
prepend-path PATH $BASE_DIR/bin
setenv SSS_ENV_DIR $LINK_DIR
setenv SSS_ENV_NAME $ENV_NAME
setenv SSS_TAG_DIR $BASE_DIR
if {"{{ env.name }}" in $EXPERIMENTAL_ENVS} {
    setenv PROJ_LIB $LINK_DIR/share/proj
}
