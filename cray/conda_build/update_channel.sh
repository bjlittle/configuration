#!/usr/bin/env bash

# conda execute
# env:
#  - python 2.*
#  - gitpython
#  - conda ==4.2.13
#  - conda-build ==1.18.2
#  - conda-build-all
#  - pip
#  - centrally-managed-conda
# channels:
#  - conda-forge
#  - defaults
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

export HOST_TYPE='desktop'
source ${SCRIPT_DIR}/env_setup.sh

# Create working directory
[ ! -d ${WORKDIR} ] && mkdir -p ${WORKDIR}

# Copy the index_channel script into the work directory, which the pbs will
# run if anything is built.
cp ${SCRIPT_DIR}/index_channel.sh ${SCRIPT_DIR}/env_setup.sh ${WORKDIR}

export CONDARC="${LOCALTEMP}/conda_cray/update_channel_condarc"
cat <<EOF >${CONDARC}

ssl_verify: False

EOF

#=============================================================================
echo -e "\nTASK: Fetch the recipes"

cat ${SCRIPT_DIR}/sources.yaml

# Fetch recipes from sources defined in yaml file
python -m centrally_managed_conda.fetch_recipes \
    ${SCRIPT_DIR}/sources.yaml --recipes-directory=${WORKDIR}/recipe_sources

#=============================================================================
echo -e "\nTASK: Flatten the collection of recipes"

# Clean up any previous composing of recipes.
rm -rf ${WORKDIR}/recipes/

# Flatten the collection of recipe sources.
RECIPES_ROOT=${WORKDIR}/recipe_sources
RECIPES=$(cat ${SCRIPT_DIR}/sources.yaml | grep "^- git_url" | rev | cut -d'/' -f1 | rev | cut -d '.' -f1 | sed "s#\(.*\)#${RECIPES_ROOT}/\*-\1\*/conda-recipes#")
python -m centrally_managed_conda.compose_recipes --output-dir=${WORKDIR}/recipes/ ${RECIPES}

#=============================================================================
echo -e "\nTASK: Update source cache"

python -m centrally_managed_conda.fetch_sources \
    --recipes-directory=${WORKDIR}/recipes \
    ${WORKDIR}/source_cache/

#=============================================================================
echo -e "\nTASK: Transfer recipes and source to CRAY"

# Transfer exactly the same content that is in desktop:${WORKDIR}/recipes and
# desktop:${WORKDIR}/source_cache, and remove anything on the target which 
# shouldn't be there (hence the delete).
rsync -av --delete ${WORKDIR}/recipes ${TARGET_REMOTE}:${CRAY_WORKDIR}
rsync -av --delete ${WORKDIR}/source_cache ${TARGET_REMOTE}:${CRAY_WORKDIR}

# The following allows recipes with git_url sources to work. Conda-build tries
# to download the source by making a "git fetch" call that fails as the Cray
# can't access the Internet. This modifies the remote information in the repo
# for each source, which circumvents the failues.
ssh -o BatchMode=yes ${TARGET_REMOTE} 'find '${CRAY_WORKDIR}'/source_cache/git_cache/ \
    -type d -iname "*.git" -exec bash -c "cd {}; git remote set-url origin ." \;'

#=============================================================================
echo -e "\nTASK: Run build on the cray"

# Move scripts that will be run on the cray.
scp ${SCRIPT_DIR}/env_setup.sh ${SCRIPT_DIR}/cray_update_channel.sh \
    ${SCRIPT_DIR}/cray_build.sh ${TARGET_REMOTE}:${CRAY_WORKDIR}/

NOW=$(date +%Y-%m-%d-%H%M%S)
CRAY_LOGFILE=${CRAY_LOGDIR}/cray_pbs_build.${NOW}.log
CRAY_OUTFILE=${CRAY_LOGDIR}/cray_pbs_build.${NOW}.out

ssh -o BatchMode=yes ${TARGET_REMOTE} "mkdir -p ${CRAY_LOGDIR}"

echo -e "\nTemporarily writing output to ${TARGET_REMOTE}:${CRAY_LOGFILE}"

# To avoid the pbs epilogue appearing in the file, we write to CRAY_LOGFILE
# within the job itself.
# We also specify CRAY_OUTFILE so that we can delete the output file PBS
# produces.
timeout 90m ssh -o BatchMode=yes ${TARGET_REMOTE} "
qsub -z -N cray_conda_build -W block=true -j oe -l walltime=00:60:00 -l select=1 -q shared -o ${CRAY_OUTFILE} <<EOF
#!/usr/bin/env bash

${CRAY_WORKDIR}/cray_update_channel.sh >${CRAY_LOGFILE} 2>&1
EOF
RC=\$?
# Wait for PBS to move the output file to where it's supposed to be.
sleep 5
cat ${CRAY_LOGFILE}
rm -f ${CRAY_LOGFILE} ${CRAY_OUTFILE}
exit \$RC"

