#!/usr/bin/env bash

# conda execute
# env:
#  - python
#  - conda-build
# channels:
#  - conda-forge
#  - defaults
# run_with: bash

trap 'echo "Aborted!"; exit 1' ERR
set -e

export HOST_TYPE='desktop'
source ${LOCALDATA}/conda_bld_cray/env_setup.sh

# Reindex the channel after new packages are added.
conda index ${CHANNEL_ROOT}
