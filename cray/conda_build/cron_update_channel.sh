#!/usr/bin/env bash

# Purpose:
#   Cron job to refresh all the builds for the 'CRAY' platform.

#=============================================================================

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Define the command we will execute.
EXECUTE="~avd/live/conda-execute/bin/conda-execute"
COMMAND="${EXECUTE} -vf ${SCRIPT_DIR}/update_channel.sh"

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Capture output to a standard logfile location.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/cray_build.$(date +%Y-%m-%d-%H%M%S).log

# Start the logfile with a descriptive header.
echo "Latest 'Cray' channel build output." >${LOGFILE}
echo "DATE: $(date)" >>${LOGFILE}
echo "COMMAND: ${COMMAND} >${LOGFILE} 2>&1" >>${LOGFILE}
echo "OUTPUT..." >>${LOGFILE}

# Define a temporary path for conda environments. conda-execute uses this
# as a basis of its own temporary environment cache.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda_cray/envs
mkdir -p ${CONDA_ENVS_PATH}

# Run the main command + capture a success flag. (unbuffer to keep stdout
# and stderr together).
CHANNEL_UPDATE="true" unbuffer ${COMMAND} >>${LOGFILE} 2>&1
RETURNCODE=${?}

RESULT=$(tail -1 ${LOGFILE})

# If anything went wrong, emit text to produce a cron email.
if [ ${RETURNCODE} -ne 0 -o "${RESULT}" == Updated ]; then
    if [ ${RETURNCODE} -ne 0 ]; then
        echo "Cray build problem from ${0} on ${HOSTNAME}"
        STATUS="FAILED with rc=${RETURNCODE}"
    else
        echo "Cray channel updated from ${0} on ${HOSTNAME}"
        STATUS=SUCCEEDED
    fi
    echo "See log file: ${LOGFILE}"
    echo "STARTS ...<<"
    head -n50 ${LOGFILE}
    echo ">>"
    echo "ENDS ...<<"
    tail -n200 ${LOGFILE}
    echo ">>"
    echo ${STATUS}
fi
