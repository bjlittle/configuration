#!/usr/bin/env bash

# The Cray machine that we are building on. 
TARGET_REMOTE=xcel00

# The location of the conda channel that we are building with.
CHANNEL_NAME="cray"
CHANNEL_URL=http://www-avd/conda/${CHANNEL_NAME}
CHANNEL_ROOT=/project/avd/live/conda/${CHANNEL_NAME}/linux-64

# Desktop specific environment
if [ "${HOST_TYPE}" = "desktop" ]; then
    WORKDIR=${LOCALDATA}/conda_bld_cray
    CRAY_WORKDIR=$(ssh -o BatchMode=yes ${TARGET_REMOTE} 'echo ${DATADIR}')/conda_bld_cray
fi

# Cray specific environment
if [ "${HOST_TYPE}" = "cray" ]; then
    CRAY_WORKDIR=${DATADIR}/conda_bld_cray
    CHANNEL_HOST=els056
    
    # The location to store built pacakages
    NEW_DISTRIBUTIONS_DIR=${CRAY_WORKDIR}/new_distributions
fi

# The location of PBS job output
CRAY_LOGDIR=${CRAY_WORKDIR}/logs
