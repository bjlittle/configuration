#!/usr/bin/env bash

#
# Purpose:
#   Cron job to refresh all the builds for the 'scidesktop' platform.
# Details:
#   Run "update_channel.sh" in the same directory as this.
#   Most of the context is stored there, including the dirpath for the local channel.
#   All we do here is to call that and report any errors.
#

# Get the directory of this script.
SCRIPT_DIR=$(cd "$(dirname ${0})"; pwd;)

# Define the command we will execute.
EXECUTE="~avd/live/conda-execute/bin/conda-execute"
COMMAND="${EXECUTE} -vf ${SCRIPT_DIR}/update_channel.sh"

# Check the availablity of SCRATCH.
PROBE=$([ -d ${SCRATCH} ] && echo "true" || echo "false")
[ "${PROBE}" == "false" ] && export SCRATCH=${LOCALTEMP}

# Capture output to a standard logfile location, so we can check the latest build run anytime.
LOG_DIR=${SCRATCH}/logs
mkdir -p ${LOG_DIR}
LOGFILE=${LOG_DIR}/scidesktop_build.$(date +%Y-%m-%d-%H%M%S).log

# Start the logfile with a descriptive header
cat <<EOF >${LOGFILE}
Latest 'scidesktop' channel build output.
DATE:    $(date)
COMMAND: ${COMMAND} >${LOGFILE} 2>&1
OUTPUT ...

EOF

# Define a temporary path for conda environments. conda-execute uses this
# as a basis of its own temporary environment cache.
export CONDA_ENVS_PATH=${LOCALTEMP}/conda/scidesktop_build/envs
mkdir -p ${CONDA_ENVS_PATH}

# Run the main command + capture a success flag. (unbuffer to keep stdout
# and stderr together)
CHANNEL_UPDATE="true" unbuffer ${COMMAND} >>${LOGFILE} 2>&1
RETURNCODE=${?}
UNCHANGED=$(tail -5 ${LOG_FILE} | grep -ic "^unchanged$")

# If anything went wrong or the channel was updated, emit text to produce a
# cron email.
if [ ${RETURNCODE} -ne 0 -o ${UNCHANGED} -eq 0 ]; then
    if [ ${RETURNCODE} -ne 0 ]; then
        echo "Scidesktop build problem from ${0} on ${HOSTNAME}"
        STATUS="FAILED with rc=${RETURNCODE}"
    else
        echo "Scidesktop channel updated from ${0} on ${HOSTNAME}"
        STATUS="SUCCEEDED"
    fi
    echo "See logfile: ${LOGFILE}"
    echo "STARTS ...<<"
    head -n50 ${LOGFILE}
    echo ">>"
    echo "ENDS ...<<"
    tail -n200 ${LOGFILE}
    echo ">>"
    echo ${STATUS}
fi
