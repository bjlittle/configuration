Name:           SciTools-RPM-updater-on-shutdown
Version:        1.0
Release:        2
Summary:        A service that runs /opt/scitools/.updater/bin/update-scitools.sh on shutdown

License:        BSD 3
Group:          Unspecified

BuildRoot:      %{_tmppath}/SciTools-RPM-updater-on-shutdown

Requires:   SciTools-RPM-updater


%description

A service that runs /opt/scitools/.updater/bin/update-scitools.sh on shutdown

%prep
# Clear up any pre-existing build-root.
rm -rf ${RPM_BUILD_ROOT}/
mkdir -p ${RPM_BUILD_ROOT}

%post
/sbin/chkconfig --add scitools-update
/sbin/chkconfig --level 2345 scitools-update on

%preun
if [ $1 = 0 ]; then
  /sbin/chkconfig --del scitools-update
fi


%install
export INIT_DIR=/etc/init.d
export INIT_FILE=${INIT_DIR}/scitools-update
mkdir -p ${RPM_BUILD_ROOT}${INIT_DIR}

echo '#!/bin/sh
#
#chkconfig --list (run-level, start priority, stop priority)
# chkconfig: 2345 65 25
# description: Run a SciTools-update at shutdown

# Thanks to https://opensource.com/life/16/11/running-commands-shutdown-linux

LOCKFILE=/var/lock/subsys/scitools-update

# Source function library.
. /etc/rc.d/init.d/functions

start(){
    plymouth message --text="Enable SciTools update on reboot"
    # Touch our lock file so that stopping will work correctly
    touch ${LOCKFILE}
    success
    return 0
}

stop(){
    plymouth message --text="Checking for Scientific Software Stack updates... Hit ESC for progress"

    # Remove our lock file
    rm -f ${LOCKFILE}

    # Run that command that we wanted to run
    /opt/scitools/.updater/bin/update-scitools.sh

    success
    return 0
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    *)
        echo "Usage: $0 {start|stop}"
        exit 1
    ;;
esac

exit 0
' > ${RPM_BUILD_ROOT}${INIT_FILE}


%files
%attr(0755, root, root) "/etc/init.d/scitools-update"

%changelog
* Wed Aug 01 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.0-2
- Added changelog

* Wed Aug 01 2018 Philip Elson <philip.elson@metoffice.gov.uk> - 1.0-1
- Initial RPM release

