Name:           SciTools-SciDesktop
Version:        1.27
Release:        1
Summary:        The scientific software stack provided as part of standard Scientifc Desktop.

License:        BSD 3
BuildRoot:      %{_tmppath}/SciTools-SciDesktop

Requires: SciTools-SciDesktop-patch
Requires: SciTools-env-default-label-next
Requires: SciTools-env-default-label-current
Requires: SciTools-env-default-label-previous
Requires: SciTools-env-default_legacy-label-current
Requires: SciTools-env-default_legacy-label-previous
Requires: SciTools-env-experimental-label-current
Requires: SciTools-env-production-label-os41-1
Requires: SciTools-env-production-label-os42-1
Requires: SciTools-env-production-label-os43-1
Requires: SciTools-env-production-label-os43-2
Requires: SciTools-env-production-label-os44-1
Requires: SciTools-env-production_legacy-label-os42-1
Requires: SciTools-env-production_legacy-label-os43-1
Requires: SciTools-env-production_legacy-label-os43-2

Obsoletes: SciTools-env-preproduction-label-os44
Obsoletes: SciTools-env-preproduction-label-os43
Obsoletes: SciTools-env-preproduction_legacy-label-os43

%description

This package installs the software environments that can be expected on the standard Met Office scientific desktop estate.

The package is entirely meta - it has no functionality other than to serve as a single point of installation
 for the Scientific Desktop estate and management of obsoleted labels and environments.

%files


%changelog
* Tue Aug 04 2020 Alastair Gemmell <alastair.gemmell@metoffice.gov.uk> - 1.27-1
- Retire preproduction-os44 
- Remove persisting preproduction-os43 and preproduction_legacy-os43 
- Note: above points done as temporary fix pending removal by Platforms

* Mon Jun 01 2020 Laura Dreyer <laura.dreyer@metoffice.gov.uk> - 1.26-1
- Add production label for os44-1

* Fri Mar 06 2020 Bill Little <bill.little@metoffice.gov.uk> - 1.25-1
- Remove SciTools-RPM-updater-on-shutdown dependency 

* Tue Mar 03 2020 Bill Little <bill.little@metoffice.gov.uk> - 1.24-1
- Add preproduction label for os44

* Mon Mar 02 2020 Bill Little <bill.little@metoffice.gov.uk> - 1.23-1
- Decommission experimental_legacy-current and default_legacy-next

* Mon Jul 29 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.22-1
- Add production+legacy labels for os43-2

* Mon Jul 15 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.21-1
- Retire production-os40-1 and preproduction os43

* Thu Jun 13 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.20-1
- Add production+legacy labels for os43-1

* Wed Apr 03 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.19-1
- Add preproduction+legacy labels for os43

* Mon Feb 25 2019 Laura Dreyer <laura.dreyer@metoffice.gov.uk> - 1.18-1
- Removed the SciTools-env-production-label-os39 requirement

* Fri Feb 15 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.17-1
- Added SciTools-RPM-updater-on-shutdown requires

* Thu Jan 17 2019 Bill Little <bill.little@metoffice.gov.uk> - 1.16-1
- Added production-os42-1 label

* Wed Oct 17 2018 Bill Little <bill.litte@metoffice.gov.uk> - 1.15-1
- Added production_legacy-os42-1 label

* Wed Aug 15 2018 Bill Little <bill.little@metoffice.gov.uk> - 1.13-1
- Added requires for default_legacy
