Name:    SciTools-logrotate
Version: 1.0
Release: 1
Summary: Perform automatic log rotation of SciTools RPM updater logs

License: BSD 3
Group:   Unspecified

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}


%description
Perform automatic log rotation of SciTools RPM updater logs.


%prep
rm -rf %{buildroot}


%clean
rm -rf %{buildroot}


%install
export LOG_DIR=/etc/logrotate.d
mkdir -p %{buildroot}${LOG_DIR}
export LOG_FILE=%{buildroot}${LOG_DIR}/scitools-update

cat <<'EOF' >${LOG_FILE}
/var/log/scitools_cron.log /var/log/scitools_updater.log {
    monthly
    rotate 6
    compress
    missingok
    notifempty
    create 0644 root root
    dateext
}
EOF


%files
%attr(0644, root, root) "/etc/logrotate.d/scitools-update"


%changelog
* Wed Feb 27 2019 <bill.little@metoffice.gov.uk> - 1.0-1
- Initial logrotate


