.. include:: globals.rst.inc

Production Use
==============

AVD Involvement
---------------

The AVD Team are involved in:

* Creating and managing all of the environments
* Creating the :term:`RPM` that are used on production
* Engaging with the Parallel Suite Project Manager to ensure ``preproduction``
  environments are provided before the parallel suite commences
* Creating the :term:`RPM` that are used for the Parallel Suite duration
* Deployment of and changes to the Parallel Suite environments via
  :term:`RPM`

.. note:: The `AVD Team`_ cannot deploy production RPMs to the HPC estate or to
          production servers. Rather, this is managed through a **ServiceNow**
          request and actioned by the `HPC Support <https://metnet2.metoffice.gov.uk/team/hpc-support>`_
          team and `Platforms <https://metnet2.metoffice.gov.uk/team/platforms>`_
          team.


Operational Release Schedule
----------------------------

To participate in the specification of Production Environments, please attend
the PS Checkpoint meetings (see
`here <http://ukmet-linux/wiki/index.php/Operational_%26_Parallel_Suite>`_ for
more information).

The AVD Team will remain engaged with the Parallel Suite Project Manager to
ensure all environments are provided and there is clear communication for all
suite participants as to any changes to environments.

For the dates for the Scientific Software Stack environment release please
see the `Scientific Software Stack Production Roadmap`_ that the AVD Team maintain.


For more information on the operational environments see
`Operational Environments <production_use#operational-use-environments>`_.



Production Linux Servers
------------------------

In the context of the Scientific Software Stack a **production** Linux server
is one that is supported by
the `Standalone Applications Team <https://metnet2.metoffice.gov.uk/team/standalone-applications>`_.
Typically these servers may have any ``production`` labelled environment
deployed via an :term:`RPM`.

Production servers typically participate in the following development to
operational tier:

   * **dev**.  Typically a Scientific Desktop linux or VDI machine, see the
     above section for what is available.
   * **tst**, **test**, **ts**.  Could be a Scientific Desktop, VDI or a
     dedicated separate Linux server using :term:`RPM` to deploy the stack.
   * **preprod**, **prod01**.  Depending on how it was built this would
     typically have an :term:`RPM` deployed so that it mirrors the **prod**
     Linux server.
   * **prod**, **op**.  Strongly recommended to use the :term:`RPM`
     to deploy the stack.


.. note:: Note that production Linux servers are never updated automatically.
          In order to update a production Linux server you will need to
          raise a ServiceNow request with the platforms team.


Using Python in Production
--------------------------

If you are using Python as part of an operational suite, you should use the
dedicated production Python environments which are tied to specific
operational suites. To access one of these environments use ``module load``.
For example, if you are running the environment for **Operational Suite 40**
you would run:

.. code-block:: bash

    $ module load scitools/production-os40-1


.. warning:: Avoid invoking a stack environment within a Python script by
             using a **hashbang** as this is will circumvent the specifically
             environment settings that ``module load`` applies.
             For this reason, it is recommended that you
             instead use ``module``.  See `best practice <best_practice>`_.



Which environment to use
------------------------

This is dictated by the target platform for your Python code.  In this case
it is for operational ``production`` use.  This is documented in the
`environment descriptions <environments#environment-descriptions>`_.

It is strongly recommended that all production use of the stack should use
the ``production`` environments.  There are two production platforms
currently:

* **HPC (XCE and XCF)**.  The production :term:`RPM`'s are created by the
  `AVD Team`_ and then deployed via the
  `HPC Support`_ Team as part of the parallel suite spin up.  Note
  that the **XCS-R** HPC platform will be kept in synch with **XCE** and
  **XCF** to ensure an easy transition.

* **Linux Servers**.  This also uses the production :term:`RPM`'s.
  Any :term:`RPM`'s that
  are available on the HPC are also available for deployment onto
  operational Linux servers.  Note that operational Linux servers are typically
  locked down so have no access to the Scientific Desktop Stack
  deployment. Deployment is usually done via an :term:`RPM` which is requested
  via   a **ServiceNow** change and the service owner should be aware.


Operational use Environments
----------------------------

Once an operational environment is made available, it **will not change**,
as it is **immutable**. This
means that if you have tested against a specific environment you can be sure
that the environment will remain the same until it is retired.

Before a parallel suite starts, a proposal for the next production environment
will be made available, referred to as the ``preproduction`` environment.
This environment is not intended for operational use and will be removed once
the ``production`` environment is deployed. After the ``preproduction`` environment
is announced, a suitable time period will be given, allowing time for changes
to be requested if needed.

Once the environment specification has been finalised, it will be deployed,
forming the first version of the environment for that suite (e.g.
``scitools/production-os40-1``).

If a change to the environment (for example due to an updated version of a package)
is required during the parallel suite, we will deploy a new environment that
contains the update (e.g. ``scitools/production-os40-2``).

We aim to minimise the number of environments that are available for each
suite so new deployments of a new environment will only occur once or twice
during a parallel suite.

After chill, we will only be able to deploy another environment under
exceptional circumstances, as agreed with the parallel suite project manager.



Retiring Old Environments
-------------------------

Once an operational environment is no longer in use, an announcement will be
made stating that the environment will be retired, and it will then be removed
permanently from the estate.

..  _AVD Team: https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam
.. _HPC Support: https://metnet2.metoffice.gov.uk/team/hpc-support
.. _OPCHANGE Trac: http://fcm1/projects/OPCHANGE/
.. _Scientific Software Stack Production Roadmap: https://exxreldocs:8099/display/AVD/Production+Environments
