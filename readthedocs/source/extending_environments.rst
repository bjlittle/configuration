.. include:: globals.rst.inc

Extending Environments
======================

In what circumstances would I need to extend an environment?

* There may be occasion when we are not able to accept a request for an addition
  to the stack
* A package has been accepted to be added but it has not be made available yet
* You want to run your own custom package or application

You can do this by linking the package to a stack environment in the
following way.


The ``PYTHONPATH`` Variable
---------------------------

Setting the ``PYTHONPATH`` variable in your shell session will add a chosen directory to the
beginning of your Python executable's search path.  It is recommended
to always set your ``PYTHONPATH`` in a **shell script** that is specific to work
or project your Python is related to, see `best practice <best_practice>`_
for more information.

Please refer to the official
`Python documentation <https://docs.python.org/2/using/cmdline.html?highlight=pythonpath#envvar-PYTHONPATH>`_
for more information on ``PYTHONPATH``.


.. warning:: Checking the ``PYTHONPATH`` variable will not always show you all
             the directories which could be searched for imports.  This is
             because you can also set those directories using the ``PATH``
             variable (this variable is not specific to Python but will
             still augment Python's search path for imports).


.. tip:: You can keep track of all ``PATH`` and ``PYTHONPATH`` variables by
         importing the Python module `sys` and then printing ``sys.path``.
         This is particularly useful when trying to identify where python
         is finding erroneous code.

         For example:

         .. code-block:: bash

            $ python -c "import sys; print('\n'.join(sys.path))"

            /opt/scitools/environments/default/2017_11_08/lib/python27.zip
            /opt/scitools/environments/default/2017_11_08/lib/python2.7
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/plat-linux2
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/lib-tk
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/lib-old
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/lib-dynload
            /home/user/python_stuff
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/site-packages
            /opt/scitools/environments/default/2017_11_08/lib/python2.7/site-packages/cycler-0.10.0-py2.7.egg
