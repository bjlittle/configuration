.. include:: globals.rst.inc

Tools
=====

Below is a summary of the tools we use and why we use it.

.. image:: images/conda-logo.svg
   :target: https://conda.io/docs/
   :alt: conda
   :width: 100px

.. image:: images/rtd-logo.png
   :target: https://docs.readthedocs.io
   :alt: Read The Docs
   :width: 100px

.. image:: images/jinja-logo.png
   :target: https://palletsprojects.com/p/jinja/
   :alt: jinja
   :width: 100px

.. image:: images/asciinema-logo.svg
   :target: https://asciinema.org/
   :alt: asciinema
   :width: 40px


conda
-----

An open source, cross platform, language agnostic package manager and
environment system. See the `https://conda.io/docs/ <https://conda.io/docs/>`_
for more information.

* `conda-build <https://github.com/conda/conda-build>`_
* `conda-build-all <https://github.com/conda-tools/conda-build-all>`_
* `conda-gitenv <https://github.com/SciTools/conda-gitenv>`_
* `conda-execute <https://github.com/conda-tools/conda-execute>`_
* `conda-rpms <https://github.com/scitools-incubator/conda-rpms>`_
* `centrally-managed-conda <https://github.com/pelson/centrally-managed-conda>`_


Documentation
-------------

We have used more than one tool:

* `Read the Docs <https://docs.readthedocs.io>`_ is used as it requires very
  little knowledge of html and is easy to maintain

* `jinja2 <https://palletsprojects.com/p/jinja/>`_ is used in
  conjunction with Read the Docs to allow for dynamically created content

* `asciinema <https://asciinema.org/>`_ is used to show a Linux termainl
  playback of commands.


.. note:: You can access `Bitbucket`_
          for this site for more information and even contribute if you wish.
