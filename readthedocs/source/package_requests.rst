.. include:: globals.rst.inc

Package Requests
================

This section describes the package request process for updates or additions to
development environments in the Scientific Software Stack.

.. note:: To make a request for a package in a production environment, please
          follow the `Operational Suite Release Schedule <production_use#operational-release-schedule>`_


Process of Requesting a Package
-------------------------------

.. figure:: images/package-request-process.png
   :align: center

   **Package Request Process**

.. note:: Please note that if the initial request to add a given package is
          rejected the package could be reconsidered later if the reason for
          rejecting it has been resolved.  An example of this may be there is
          a dependency that clashes with an existing package, which is later
          resolved.


Request of a package made by user
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please check our `decision log`_ for
existing requests, and providing your package is not already pending, contact
`AVD Support <mailto:ml-avd-support@metoffice.gov.uk?subject=Scientific Software Stack Package Request>`_
with the following details:

* Name of the Python package
* Name of the requester
* Reason for requesting the package
* Date needed by

The AVD Team will then create a decision log entry to capture additional
information.


Package request added to decision log
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The request will be added to a backlog of requested packages `decision log`_ on Twiki,
each of which will be approved or rejected as per our
`package policy <package_policy>`_.


AVD Evaluates the request
^^^^^^^^^^^^^^^^^^^^^^^^^

The Scientific Software Stack requires constant attention to ensure the
correct packages are available at the right time for research and
production use.  Part of this is to monitor changes in the
packages that constitute the :term:`environment` in order to avoid
dependency failures.

The evaluation that occurs at this point is to ensure the requested
:term:`Python package` **should** and **could** exist in the Stack and
there are no other known alternatives.  There
needs to be some moderation to ensure this aspect.  If the evaluation outcome
is not obvious there will be some engagement with the requester and perhaps a
more open forum such as Yammer to establish dialog.


Add to decision log
^^^^^^^^^^^^^^^^^^^

In order to monitor the progress of the addition to the Stack an internal AVD
Team ticket will be created.


Announce addition of package and planned release
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When a package has been added the requester will be updated and an
announcement made via the
`AVD Yammer group <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_
detailing the package (or packages) added and when to expect an updated environment.


How long will it take to get a package added?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This depends on the priority of the request.  The AVD Team are
continuously looking to improve the Python tools that are provided
and there may be other priorities.  If a package is of particular
importance to you please ensure that you state this when requesting it.

It is highly unlikely that new packages will go from approval into the
``default`` or ``production`` environments immediately. Instead, new packages
will typically be added to the ``experimental`` environment for longer term
evaluation.


Decision log of Requested Packages
----------------------------------

A `decision log`_
for package requests is kept in TWiki, and we expect this to be fairly
active. Using TWiki provides a means to react efficiently to requests and
changes.


How can I use a requested package before it's been added to the Stack?
----------------------------------------------------------------------

If you would like to use a particular package in your working environment but
it has not been added to the Stack, you can do so by following the standard python
package installation process on a user-by-user basis by
adding a directory to your ``PYTHONPATH`` variable.  Please ensure compliance with
SyOps and be aware that this approach may be complex for anything but pure Python
pacakages with few dependencies.
Please refer to our `Extending Environments <extending_environments>`_ and
`Best Practices <best_practice>`_ guides for details.


How can I help the AVD Team get a package into the Stack?
---------------------------------------------------------

If you would like add a package yourself to the Stack or help with getting a
package added please contact the `AVD Team <contact>`_.


.. _decision log: http://www-twiki/Gfx/SSSDecisionLog
