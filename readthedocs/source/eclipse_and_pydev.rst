.. include:: globals.rst.inc

Python Debugging
================


Python Debugging with Eclipse
-----------------------------

`Eclipse <https://en.wikipedia.org/wiki/Eclipse_(software)>`_ is a general-purpose integrated desktop environment (IDE) for code editing and debug.
Several versions are installed on the scientific desktop, see
see `Eclipse on the desktop <http://www-twiki/Desktop/LinuxSoftwareInventoryEclipsePluginsRH6>`_.

To use Eclipse with Python you need to install the **PyDev** plugin,
see `PyDev for Eclipse on RHEL6 <http://www-twiki/Desktop/LinuxUGEclipsePyDevRH6>`_
and `PyDev for Eclipse on RHEL7 <http://www-twiki/Desktop/LinuxUGEclipsePyDevRH7>`_

.. note:: Eclipse 4.4 and 3.7 work well with PyDev, the earlier 3.6 version
             is not recommended.


Configuring to use an SSS environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**First**, you need to determine the absolute path to the desired environment.
You can do that with the commands ``module load scitools/<environment_name>``
and then ``which python``.

For example :

.. code-block:: bash

    $ module switch scitools/default-next
    $ which python
    /opt/scitools/environments/default/2018_01_19/bin/python

This shows the absolute executable path which identifies the deployed Python
environment.

**Secondly** go into Eclipse and create a new interpreter.
Enter the path determined above, to reference the specific Python executable
for the desired environment.  This process is explained at
`Eclipse interpreter setup <http://www.pydev.org/manual_101_interpreter.html>`_

**Thirdly** switch existing code projects to use that interpreter, as required.
This can be controlled from right-clicking on the project and selecting
:guilabel:`Properties` > :guilabel:`PyDev - Interpreter`.
See the account of this in
`PyDev for Eclipse on RHEL6 <http://www-twiki/Desktop/LinuxUGEclipsePyDevRH6>`_
and `PyDev for Eclipse on RHEL7 <http://www-twiki/Desktop/LinuxUGEclipsePyDevRH7>`_.


Effect on Eclipse When the Available Environments Change
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Eclipse **snapshots** all the available import paths when creating an
interpreter, and converts them all to absolute paths.  Because of that,
Eclipse can't be made to follow the environment labels, or the matching soft
links in the filesystem.

When tagged environments change, as explained in
`Label Transitions <environments#label-transitions>`_, generally no change
will be seen in Eclipse.  However, you will eventually want to upgrade to
use newer environments.  In that case, much the safest and simplest way is
simply to delete the old interpreter in Eclipse and make a new one.

All previous environments will persist for some time at their fixed
(date-named) paths, but for space reasons they will eventually be removed.
See `Environment Availability <environments#environment-availability>`_.
