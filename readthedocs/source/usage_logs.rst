.. include:: globals.rst.inc

Usage Logs
==========

In order to understand how Python is being used in the Met Office all
use of the Scientific Software Stack environments are centrally logged.
These log files are then used to visualise the usage to understand trends
and to potentially inform active users of upcoming changes.


* `All logging Jupyter notebooks <http://www-avd/nbviewer/MetOffice/file_/project/avd/python/usage_logs/notebook>`_.
  This includes some daily and monthly notebooks.
* `Usage over time <http://www-avd/nbviewer/MetOffice/file_/project/avd/python/usage_logs/notebook/python_usage_over_time.ipynb>`_.
  This includes:

    * Daily Python invocations over time for minor and major Python versions
    * List of users using Python2
    * List of users using Python3

* `Scientific Software Stack Environment usage <http://www-avd/nbviewer/MetOffice/file_/project/avd/python/usage_logs/notebook/SSS_env_usage.ipynb>`_.
  This includes:

    * Number of users and total python invocations by environment
    * Number of unique users by environment
    * Total invocations by environment
    * Users by environment


If you have any questions about the Python logging please get in touch, see
our  `contact <contact>`_ details.
