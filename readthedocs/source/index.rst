.. include:: globals.rst.inc

Scientific Software Stack Documentation
=======================================

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Getting started

   getting_started
   best_practice
   troubleshooting
   cheat_sheet
   roadmap
   feedback


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Environments
   :glob:

   environments
   environments_available
   environments_compare
   extending_environments
   platforms
   production_use


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Packages

   package_policy
   package_requests
   sss-browser/autogen-sss-packages-used


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Help & Reference

   usage_logs
   testing
   faq
   eclipse_and_pydev
   glossary
   tools
   contact


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Related Documentation

   Dask Best Practice <http://www-avd/sci/dask_best_practice>


.. image:: images/sss-logo.png
   :align: right


The Scientific Software Stack is a collection of **Python-based**
software environments that deliver scientific libraries to Met Office Linux
systems.  The aim is to provide sufficiently up to date environments to allow
users (predominantly scientists) to use modern Python tools that are consistent
across all platforms.

A range of libraries in numerous languages are provided in the Scientific
Software Stack, with a primary programming interface in Python. It supports
programming with widely used tools such as `iris`_, `matplotlib`_, `numpy`_
and specialised Met Office tools.

The Scientific Software stack is available on the scientific desktops,
servers, and the Cray XC40 HPC. It contains a number of software
environments, each designed for a specific purpose (e.g. production).
For more information on the platforms see
`Environment Browser <environments_available>`_.


Stack Contents
--------------

The Scientific Software Stack provides libraries which are contained
in a set of environments.  The environments provided by the Stack are
listed below:

* ``default-next``
* ``default-current``
* ``default-previous``
* ``default_legacy-next``
* ``default_legacy-current``
* ``default_legacy-previous``
* ``experimental-current``
* ``experimental_legacy-current``
* ``preproduction-osXX``
* ``production-osXX-X``

For more details of the environments available for each platform
and their contents please see the
`Environments Browser <environments_available>`_.

Each environment has its own particular intended use. The contents of the
environments (libraries and versions of libraries) differ based on their intended
purpose, though they are all similar in content.

The update frequency of an `environment <environments#environment-description>`_
is dependent on its intended purpose.  For each
`platform <platforms>`_ there is a 
`deployment method and window <platforms#deployment-method-window>`_ for
updates.


The name of the production environment is comprised of the name of the Operational
Suite that it is associated with, followed by the build number.
For example, the production environment for os40 will be called ``production-os40-1``.

More detail on Stack environments is available on the
`environments <environments>`_ page.



.. _iris: https://github.com/SciTools/iris
.. _matplotlib: https://github.com/matplotlib/matplotlib
.. _numpy: https://github.com/numpy/numpy
