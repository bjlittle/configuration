.. include:: globals.rst.inc

Contact
=======

* For queries related to the Scientific Software Stack please contact
  `AVD Support <https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam/SitePages/AVD-Support.aspx?web=1>`_
* See the `AVD Yammer group <https://www.yammer.com/metoffice.gov.uk/topics/28360539#/Threads/AboutTopic?type=about_topic&feedId=28360539>`_ for announcements.
* For more information about the AVD Team please see our
  `SharePoint Team Page <https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam>`_
* For information on our events and resources, please visit the
  `AVD SharePoint <https://metoffice.sharepoint.com/sites/TechSolDelAVDTeam>`_ site
