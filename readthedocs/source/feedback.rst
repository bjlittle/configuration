.. include:: globals.rst.inc

Feedback
========

The AVD Team welcomes any feedback on the content or presentation of this
documentation, see our  `contact <contact>`_ details.


.. note:: You can access `Bitbucket`_
          for this site for more information and even contribute if you wish.
