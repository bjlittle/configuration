.. include:: globals.rst.inc

Roadmap
=======

This section details planned Scientific Software Stack features or maintenance
activities that are not just environment package management.

Further agreed plan details will be added appropriately in due time.  If you
have any concerns or want to know more information then please
`contact us <contact>`_.


Python3 Environments
--------------------

Python3 environments have been available across the estate within the
Scientific Software Stack since **April 2018**.


RHEL7 Python Environments
-------------------------

The Scientific Software Stack has been available on RHEL7 since
**September 2017**.


VDI Python Environments
-----------------------

The Scientific Software Stack is available for both RHEL6 and RHEL7 VDI
instances.


Python2 Deprecation & Decommissioning Roadmap
---------------------------------------------

.. important:: As of **01/01/2020** Python2 will be
               `End Of Life <https://pythonclock.org/>`_.
               There will be **no** Python2.8. The official upgrade path is
               from Python2.7 to Python3. For further information,
               see `PEP 373 <https://www.python.org/dev/peps/pep-0373/>`_ and
               `PEP 404 <https://www.python.org/dev/peps/pep-0404/>`_.

Agreement
^^^^^^^^^

* This Python2 deprecation and decommission roadmap was presented and
  provisionally agreed with UM Model owners at the PS Checkpoint meeting
  on **01/04/2019**, along with Kevin Wheeler (Parallel Suite Project Manager).
* On **27/06/2019** the Technology Management Group (TMG) endorsed the need
  to deprecate and decommission Python2.
* On **04/09/2019** an agreement with Technology and Science was 
  `announced on metnet <https://metnet2.metoffice.gov.uk/news/python2-deprecation-decommissioning>`_ 
  supporting the Python2 deprecation and decommissioning.

.. note:: The Python2 deprecation and decommissioning applies to the
          Scientific Software Stack across the entire Met Office
          estate.  For a list of platforms deployed to see
          `here <platforms>`_.

The Python2 deprecation and decommissioning roadmap for the Scientific
Software Stack across the Met Office estate is as follows:


2019
^^^^

:20 May 2019:

* Release for OS43.  This timeframe aligns with the Parallel Suite
  `OSAG <https://metoffice.sharepoint.com/sites/operationalscienceassurancegroup>`_
  sign off
* Deploy ``production_legacy-os43-1`` (Python2)
* Deploy ``production-os43-1`` (Python3)
* This will be the **last** Python2 ``production`` environment
* The ``production-os40-1`` (Python2) environment will be decommissioned


:1 December 2019:

* **All** Python2 **development** environments will be **chilled** i.e.,
  they will only be updated by exception.  This includes:

  * ``experimental_legacy-current``
  * ``default_legacy-next``
  * ``default_legacy-current``
  * ``default_legacy-previous``


2020
^^^^

:January 2020:

* Python2 is officially at `End-Of-Life <https://www.python.org/dev/peps/pep-0373/>`_
* **All** Python2 **development** environments will be **frozen** i.e.,
  they will not be updated
* Intent to deploy ``experimental-current`` (Python3 and Iris 2.4 and will include
  features required at OS44)


:February 2020:

* **All** Python2 **development** environments will be **decommissioned** from
  the estate except for ``default_legacy-current`` and
  ``default_legacy-previous``.  This includes:

  * ``experimental_legacy-current``
  * ``default_legacy-next``


:March 2020:

* Deploy ``preproduction-os44``


:April 2020:

* Release for OS44.  This timeframe aligns with the Parallel Suite
  `OSAG <https://metoffice.sharepoint.com/sites/operationalscienceassurancegroup>`_
  sign off
* Deploy ``production-os44-1`` (Python3). This will be the **first**
  Python3 only ``production`` environment.


:November 2020:

* OS44 go live
* Decommission ``production-os41-1`` (Python2)


2021
^^^^

:January 2021:

* Deploy ``preproduction-os45``


:April 2021:

* Release for OS45.  This timeframe aligns with the Parallel Suite
  `OSAG <https://metoffice.sharepoint.com/sites/operationalscienceassurancegroup>`_
  sign off
* Deploy ``production-os45-1`` (Python3)


:3 May 2021:

* **16 months** since the Python2 end of life, the final Python2
  **development** environments ``default_legacy-current`` and
  ``default_legacy-previous`` will be **decommissioned** from the estate.


:November 2021:

* OS45 goes live
* Decommission ``production_legacy-os42-1`` (Python2)
* Decommission ``production-os42-1`` (Python3)


2022
^^^^


:Early 2022:

* Suites ported to Next Generation HPC
* Release for OS46.  This timeframe aligns with the Parallel Suite
  `OSAG <https://metoffice.sharepoint.com/sites/operationalscienceassurancegroup>`_
  sign off
* Deploy ``production-os46-1`` (Python3)
* There are **no** Python2 environments on the Next Generation HPC


:July 2021:

* OS46 goes live
* Decommission ``production_legacy-os43-1`` (Python2)
* Decommission ``production_legacy-os43-2`` (Python2)
* Decommission ``production-os43-1`` (Python3)
* Decommission ``production-os43-2`` (Python3)
* There is now **no** Python2 ``production`` environments available
  on the estate.
* There have been **no** Python2 **development**
  environments on the estate since **May 2021**


:September 2022:

* End of Life current HPC


Considerations
^^^^^^^^^^^^^^

* ``Production`` environments must be deployed approximately
  **four-to-five months** before the associated operational UM Model is
  released
* The **three most recent production** environments will always be available
  on the estate
* Older ``production`` or **development** environments can be deployed
  by exception on request

As ``production-os44-1`` will be the **first** Python3 only ``production``
environment, AVD will provision the ``preproduction-os44`` environment
**earlier** for an **extended** period to allow **Python3** UM Model owners
sufficient time for additional development and testing.


Caveats
^^^^^^^

The caveat to this roadmap is the availability of fully functional
**Python3 GRIB support** through the ECMWF
`ecCodes <https://confluence.ecmwf.int/display/ECC/Python+3+interface+for+ecCodes>`_
package. Following a visit by ECMWF to the Met Office in
Easter 2019, Python3 ecCodes is currently being
`trialled in a testing environment <https://www.yammer.com/metoffice.gov.uk/threads/192481723138048>`_.
The results of the testing will be communicated to the community, and the
Python2 deprecation and decommissioning roadmap will be realigned appropriately,
if necessary.

We are aiming to include `ecCodes <https://confluence.ecmwf.int/display/ECC/Python+3+interface+for+ecCodes>`_
and `iris-grib <https://github.com/SciTools/iris-grib>`_ in OS44.
