.. include:: globals.rst.inc


Glossary
========

Stack Related
-------------

.. glossary::
      conda
         An open source, cross platform, language agnostic package manager and
         environment system.  See the `conda website <https://conda.io/docs/>`_
         for more information.

      conda channel
         Conda packages are downloaded from remote channels, which are URLs
         to directories containing conda packages.   AVD maintain and use internal
         private channels for the Scientific Software Stack environments but also
         make use of public channels.  Each conda channel has a corresponding
         **repodata.json** file that includes information about all packages available
         on the channel.

      deploy
         Means to centrally provision and make available on the
         target `platforms <platforms>`_ as part of the Scientific software
         Stack.

      environment
         A self-contained deployment of compiled software packages that are
         designed and built to work together.

      env
         Abbreviation for `environment`

      environment description
         An *environment description* has a well defined purpose.  It also
         provides a loose definition of the software that environments deriving
         from this description should contain. `default` and `production` are
         two examples of `environment descriptions`.

      environment tag
         A fully defined and immutable software environment.
         ``default-2020_12_25-1`` is an example of a tag.

      environment label
         A named environment that references an *environment tag*.  An
         environment *label* changes with time by referencing different
         environment *tags*. `default-next` is an example of an environment
         label.


      manifest
         An :term:`environment` has a manifest.  It is complete list of
         packages that are present in the :term:`environment` along with the
         corresponding :term:`conda channel` for that package.

      operational
         Servers where production quality applications are run and supported.

      preproduction
         This is a development environment provisioned prior to the associated operational
         production environment of an Operational Suite. It is intended to be representative
         of the final immutable production environment, and is available for parallel suite
         development and testing purposes.

      production
         Synonym for operational

      Python package
         A Python package is a collection of Python modules.  A module is a
         single Python file where as a package is a directory of Python
         modules.  For more detail see the official
         `Python docs <https://docs.python.org/3/tutorial/modules.html#packages>`_.

      Scientific Software Stack
         A collection of Python-based software environments
         that deliver powerful scientific libraries to Met Office Linux
         Systems.

      Stack
         Abbreviation of `Scientific Software Stack`


Misc
----

.. glossary::
     HPC
        The Met Office's high performance super-computer(s) (currently
        Cray XC40)

     RHEL6
        Acronym for Red Hat Enterprise Linux version 6.  RHEL is the standard
        linux operating system used in the Met Office.

     RPM
        RPM is a package manager, originally from Red Hat (hence the acronym
        RPM).  This is preferred for deploying to operational servers as it
        is controlled and maintainable.

If there are any definitions missing from this glossary please let us know by
`contacting <contact>`_ AVD Support.
