"""
Produce a summary of the Scientific Software Stack environments based upon the
state of the bitbucket repo.

API Doc:
* https://developer.atlassian.com/bitbucket/server/docs/latest/how-tos/command-line-rest.html
* https://developer.atlassian.com/bitbucket/api/2/reference/resource/

"""

import logging
import inspect
import requests
import re
# this disables the ""InsecureRequestWarning" when using https to query
# bitbucket.  This ison the internal network so not an issue.
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Globals.
API_URL = ('https://exxgitrepo:8443/rest/api/1.0/projects/SSS/repos/{repo}/'
           'browse/labels?at={query}')
RAW_LABEL_URL = ('https://exxgitrepo:8443/projects/SSS/repos/{repo}/'
                 'raw/labels/{fname}?at={query}')
RAW_URL = ('https://exxgitrepo:8443/projects/SSS/repos/{repo}/'
           'raw/{fname}?at={query}')
QUERY_STR = 'refs/{git_type}/{desc}'


def autolog_info(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Dump the message + the name of this function to the log.
    logging.info(" --> %s" % (
        message
    ))


def autolog_debug(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Get the previous frame in the stack, otherwise it would
    # be this function.
    func = inspect.currentframe().f_back.f_code
    # Dump the message + the name of this function to the log.
    logging.debug("%s in %s:%i --> %s" % (
        func.co_name,
        func.co_filename,
        func.co_firstlineno,
        message
    ))


def query_bb_api(repo, desc, res_type, fname=None):
    """
    Query a URL (API or not) and return the response in json or text format.

    """
    query_str = QUERY_STR.format(git_type='heads', desc=desc)
    if res_type == 'json':
        url = API_URL.format(repo=repo, query=query_str)
    elif res_type == 'text':
        url = RAW_LABEL_URL.format(repo=repo, fname=fname, query=query_str)
    else:
        raise NotImplemented

    # Need verify=False to avoid SSL issue (BitBucket cert is not signed).
    resp = requests.get(url, verify=False)
    resp.raise_for_status()

    # TODO: the json response may be paginated.  Needs to handle it.
    if res_type == 'json':
        result = resp.json()
    elif res_type == 'text':
        result = resp.text.strip()
    else:
        raise NotImplemented

    return result


def query_bb_api_for_tags(url, res_type, fname=None):
    """
    Query a URL (API or not) and return the response in json or text format.
    """

    query_str = url

    # Need verify=False to avoid SSL issue (BitBucket cert is not signed).
    resp = requests.get(url, verify=False)
    resp.raise_for_status()

    # TODO: the json response may be paginated.  Needs to handle it.
    if res_type == 'json':
        result = resp.json()
    elif res_type == 'text':
        result = resp.text.strip()
    else:
        raise NotImplemented

    return result


def query_text_url(url_str):
    """
    Query a URL (API or not) and return the response in a list of dicts

    """

    resp = requests.get(url_str, verify=False)
    resp.raise_for_status()
    result = resp.text.strip()

    return result


def query_json_url(url_str, api_user, api_key):
    """
    Query a URL (API or not) and return the response in a list of dicts.
    If the url_str is Artifactory Cloud then use the credentials.

    :param url_str: URL to use
    :param api_user: Artifactory api user
    :param api_key: Artifactory api key
    """

    if api_user and api_key:
        msg = "        Using API credentials (api_user={}) (api_key={}...{})"
        autolog_info(msg.format(api_user, api_key[:4], api_key[-4:]))

        auth = (api_user, api_key)
        resp = requests.get(url_str, verify=False, auth=auth)
    else:
        resp = requests.get(url_str, verify=False)

    resp.raise_for_status()
    result = resp.json()

    return result


def get_envs_from_rpm_list(rpm_list):
    """
    Using the rpm list populate the env details.

    :param rpm_list: list of dicts
    :param env_list: list of dicts
    """

    labels = []

    for rpm in rpm_list:
        # the "desc" and "label" should already be present
        autolog_debug("====> desc=%s, label=%s" % (rpm['desc'], rpm['label']))

        # list of repos to check
        api_platform_list = ['Scidesktop-environments', 'environments']

        for api_platform in api_platform_list:
            autolog_debug("========> api_platform=%s" % api_platform)
            # query API for a dict of files "tags".txt
            # "https://exxgitrepo:8443/rest/api/1.0/projects/SSS/repos/environments/browse/labels?at=refs%2Fheads%2Fdefault"

            url = ("https://exxgitrepo:8443/rest/api/1.0/projects/SSS/repos/{}"
                   "/browse/labels?at=refs%2Fheads%2F{}")
            query_str = url.format(api_platform, rpm["desc"])

            try:
                file_dict = query_bb_api_for_tags(query_str, 'json')
            except requests.exceptions.HTTPError:
                autolog_info("Exception handled "
                             "(requests.exceptions.HTTPError) "
                             "{}".format(query_str))
                continue

            # for each file found
            for values_dict in file_dict['children']['values']:
                file_name = values_dict['path']['name']
                file_name_no_ext = values_dict['path']['name'].rsplit(
                    '.', 1)[0]

                msg = "File found = {} (noext = {})"
                autolog_debug(msg.format(file_name, file_name_no_ext))

                # if the file_name matches a tag in the rpm then populate the
                # manifest info.

                autolog_debug("%s == %s ?" % (file_name_no_ext, rpm['label']))

                if file_name_no_ext == rpm['label']:
                    # get the env tag of what was actually deployed
                    # (found in a text file).
                    env_tag = query_bb_api(api_platform, rpm['desc'], 'text',
                                           fname=file_name)

                    # get the release date from the env tag.
                    released = env_tag.split('-')[2].replace('_', '-')

                    # Get URLs for the label's manifest and spec files.
                    manifest_query = QUERY_STR.format(git_type='tags',
                                                      desc=env_tag)
                    manifest_url = RAW_URL.format(repo=api_platform,
                                                  fname='env.manifest',
                                                  query=manifest_query)

                    rpm['manifest_url'] = manifest_url
                    rpm['env_tag'] = env_tag
                    rpm['released'] = released

    return sorted(rpm_list, key=lambda k: k['desc'])


def load_env_rpm_list():
    """
    Return a list of the environments that are defined as RPMs.

    :return rpm_list: List of RPMs (SSS envs)
    """

    scitools_scidesktop_spec_url = "https://bitbucket:8443/projects/SSS/repos/configuration/raw/scidesktop/SPECS/SciTools-SciDesktop.spec?at=refs%2Fheads%2Fmaster"
    autolog_info("Retrieving RPM list from %s" % (scitools_scidesktop_spec_url))

    # line format expected (there are many)
    #
    # Requires: SciTools-env-default-label-next
    # Requires: SciTools-env-default-label-current
    # Requires: SciTools-env-default-label-previous

    # Look for "^Requires: SciTools-env-"

    scitools_scidesktop_spec_str = query_text_url(scitools_scidesktop_spec_url)
    search_str = "^Requires: SciTools-env-"

    rpm_list = []

    for aline in scitools_scidesktop_spec_str.splitlines():
        m = re.match(search_str, aline)

        if m:
            start_index = len(search_str)-1

            # need to handle the "os43-2" so cannot just split on "-".
            # Set max of 2
            values_list = aline[start_index:].split("-", 2)

            # assuming the spec file is has consistent formatting and naming
            env_to_add = {"desc": values_list[0],
                          "label": values_list[2],
                          "released": "",
                          "env_tag": "",
                          "manifest_url": "",
                          "packages": [],
                          "unique_name": "",
                          "python_version": ""
                          }

            rpm_list.append(env_to_add)

    return rpm_list


if __name__ == "__main__":
    get_envs()
