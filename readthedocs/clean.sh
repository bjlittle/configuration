#!/usr/bin/bash

echo "Clean the jinja templates output..."
rm -f source/autogen*
rm -f source/sss-browser/autogen*

echo "Clean the generated static html & json..."
rm -f source/_static/autogen*
rm -f source/_static/environments*.json

# clear the build output from the readthedocs make
make clean
