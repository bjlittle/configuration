# Scientific Software Stack Documentation

Documentation for the Scientific Software Stack is constructed using
https://readthedocs.org that uses
[reStructuredText](https://en.wikipedia.org/wiki/ReStructuredText)
 (`.rst` files).  **jinja2** templating is also used to populate
 some `.rst` files.

Use the AVD server to view this rendered operational documentation at
http://www-avd/sci/software_stack


## Artifactory Cloud Authentication

The Environment Browser build process (`sss-browser.py`) needs to fetch the
`repodata.json` from conda channels which are provided in Artifactory.

As such, you must provide Artifactory with authentication, i.e. an
API user and key :  In fact, these are needed for both Artifactory Cloud *and*
the on-premise Artifactory, as envs are currently stored in both places.

As of **12/08/20** the configuration for the api keys has been extended to use
a json file to allow for multiple keys.

The default behaviour for the `build.sh` is to use the api credentials
in `$HOME/.artifactory/api_keys.json`.  You can also use a command line
option `--api_keys` to point to a different location for example::

`./build.sh --api_keys foo/api_keys.json`


## Example api_keys.json file

This example shows only two entries but you can add more if necessary.  Note
that the **comment** value is not used, it is purely a comment. 

    [
          {
             "comment" : "on site artifactory",
             "host"    : "https://exxmavenrepo:8443/artifactory",
             "user"    : "<first.last>",
             "key"     : "<API KEY>"
          },
          {
             "comment" : "cloud artifactory",
             "host"    : "https://metoffice.jfrog.io/metoffice",
             "user"    : "<first.last>@metoffice.gov.uk",
             "key"     : "<API KEY>"
          }
    ]

Note that the format of the user key is subtly different for the cloud and
on-premise accounts.

## Artifactory Cloud, get an API KEY

* Goto the instance of artifactory, for example
  https://metoffice.jfrog.io/metoffice/webapp/#/home.  You may 
  automatically authenticate if you are logged into the CDN.
* Select your username from the top right corner, and go to your "profile"
* Copy your API KEY, or if not present generate one (icon next to it)
* Copy your key into your `api_keys.json` file.  Ensure the permissions
  are read only to you as this is **your** api key only. 


## Developer Quickstart

Assuming you are in the same directory (readthddocs) as the clean and build 
scripts the following commands will get you going.

* `./clean.sh`.  Ensure a clean build
* `./build.sh`.  This will use the default location of the api_keys.json file.
  Utilise ` --use_cache` to optionally create/use the local cache
* `make html`.  Skips the environment browser content generation.  This is
  much faster if you are just making documentation changes.
* `make linkcheck`.  Checks all the links.  see `conf.py` for exclusions 
  (linkcheck_ignore)

  
## Building the docs

The Scientific Software Stack docs are built using the shell script `build.sh`.
This is run automatically using a
[cron job on `els056`](https://exxgitrepo:8443/projects/AVD/repos/crons/browse/crontabs/els056.avd.crontab#97-105)
to maintain continuous deployment of the latest docs.

You can build the docs manually by running `build.sh` locally.
Running `build.sh` requires a Python environment, conda execute is used in 
`build.sh` to provision this.  Note that `env-sss-rtd.yml` also contains 
the environment definition.

You can use a command line option to force use of the cache when developing. See 
the **Developer Quickstart** section for more info.

The following steps happen when running `build.py`:

* Ensure the packages are present that are needed (pip install ...)
* Clean the output from the last build if present
* Run `sss-browser.py` to create dynamic html from the corresponding jinja
  templates (`source-template `) ready for the readthedocs make to include it.
* Other files in the `source` folder are copied to the equivalent location in the
  build folder.
* The build folder is created (`build` by default).  


## Adding to the docs

* The Scientific Software Stack docs are designed to be **reStructuredTest** for
  the sake of simplicity when maintaining the docs.

* All new documentation must be added as **ReStructuredText**.  It may contain
  jinja templating elements if required, and may include html elements only if
  using html provides a better solution than can be made with pure
  **reStructuredText**.
  

## Files and Folders

The following files and folders are maintained in the Scientific Software Stack docs:

### Source & Scripts

* `source`.  Contains all the source files, mainly `*.rst` files that
  create the docs.  New and updated documentation must be added here.
* `source-template`.  Directory holding all the templates used to create
  `.rst` files.  **jinja2** is used for templating.
* `build.sh`. Generates **reStructuredText**, runs **jinja2** and renders the source **reStructuredText** as built
  html.
* `query_stack_envs.py` functions users to a summary of current Scientific Software Stack
  environments by interrogating the bitbucktet:
   * Retrieve [RPM list](https://bitbucket:8443/projects/SSS/repos/configuration/raw/scidesktop/SPECS/SciTools-SciDesktop.spec?at=refs%2Fheads%2Fmaster) 
   * Using the RPM list fill in the tag info (including the manifest url) by interrogating [Environments](https://exxgitrepo:8443/rest/api/1.0/projects/SSS/repos/environments/browse/labels?at=refs%2Fheads%2Fdefault)
      and then [Scidesktop-envrionments](https://exxgitrepo:8443/rest/api/1.0/projects/SSS/repos/Scidesktop-environments/browse/labels?at=refs%2Fheads%2Fdefault)
* `sss-browser.py` Makes a call to `query_stack_envs.py` and then retrieves 
  the manifest and constructs data structures holding env lists, env details, 
  packages and package versions.  All sss-browser generated files are in a
  sub directory names `build/sss-browser`.
     * This program will also create two json files:
        * ```http://www-avd/sci/_static/environments.json```.  Environment 
          details.
        * ```http://www-avd/sci/_static/environments-full.json```.  Environment
          details including the full package list per environment.

### Cheat Sheet

These documents includes a cheat sheet that are in this bitbucket project. This
source is in **pptx** (PowerPoint) file format.  

When making changes please ensure to manually update the date and version number
(top section of the cheat sheet).

The **pptx** files must be opened and exported to the ``source`` directory
to create the **png** and **pdf** formats.

1. Open the **pptx** file
2. Select File -> Export -> Create PDF/XPS
3. Select Publish (Defaults to same name with a new extension)
4. Select File -> Export -> Change File Type -> PNG -> Save As
5. Select Save -> "Just This One" (if you select "All Slides" it may fail to export)

It is easier to edit and export the powerpoint slides using Windows, consider
using git on a windows desktop to do this as it makes it easier.

### Development Tips

 * Command line option `--use_cache`  will use the cache or create one if there
   is not one already present.  This avoids hitting bitbucket and the conda 
   channels
 * `build`.  This folder is created from the build.  It contains the generated
  documentation.  For development purposes, you can quickly view your source
  via a web browser if you  create a symbolic link.  For example:
  `ln -s ~/configuration/readthedocs/build/html/ ~/public_html/sss-rtd`. 
  You can then view the built html via `http://www-avd/[USER]/sss-rtd`


##  Asciinema

The asciinema (https://asciinema.org/) tools allows the recording in plain
text format of a linux terminal.  This is particularly useful for informing
users how a command should be used as well as the output of the command.

When building this documentation the asciinema `.cast` files (the recordings)
are only copied into the correct folder, they are not re-recorded.  All
recordings must be made and then added to the `source/_static` directory.

To install asciinema use `pip`.  The `build.sh` script will install this 
for you in the active conda environment. Asciinema does *not* need to be 
installed to build however.

The commands to make a recording is: 

`asciinema rec foo.cast`

To embed the recording you must use the `.. raw:: html` rst command.
Example html to include:

    <head>
      <link rel="stylesheet" type="text/css" href="_static/asciinema-player.css" />
    </head>

    <script src="_static/asciinema-player.js"></script>

    <asciinema-player src="_static/foo.cast"
                      cols="105"
                      idle-time-limit=3
                      font-size="small"
                      loop="yes"
                      autoplay="yes">
    </asciinema-player>
    
As there is a copy of the `asciinema-player.css` and `asciinema-player.js`
in the `_static` directory the pip install has been pinned.  This can be
pinned to a later version but the `.css` and `.js` should also be updated
via https://github.com/asciinema/asciinema-player/releases.  Pinning to a
version could be avoided by using the github URL for the release
versions but that would be an external dependency.


## Diagrams

Diagrams are created using
[LucidCharts](https://www.lucidchart.com/invitations/accept/26090712-4c50-4624-a9d3-2c244872e74f)

