"""
A program to create .rst files that renders the SSS environments
and it packages in a readable and explorable way.

Data is sourced from an RPM list and then inferred from bitbucket.  The
conda channels are also queried for the corresponding repodata.json.

Jinja is then used to create all the output based upon templates.
"""

import argparse
import logging
import inspect
import re
import pickle
import os
import difflib
import jinja2
import time
import json
import copy
from jinja2 import FileSystemLoader
from datetime import datetime
from query_stack_envs import (
    get_envs_from_rpm_list,
    query_text_url,
    query_json_url,
    load_env_rpm_list,
)


# can be set via the command line
USE_CACHE = False

# default location for the api_keys.json
DEFAULT_API_KEYS_FORMAT = "{}/.artifactory/api_keys.json"

# static cache filename - could be a command line argument
FILE_CACHE = "sss-browser-data.p"

# set logging level (NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL)
logging.basicConfig(level=logging.INFO)

# template folders for jinja
TEMPLATE_INPUT_DIR = "source-template"
TEMPLATE_OUTPUT_DIR = "source/sss-browser"
STATIC_OUTPUT_DIR = "source/_static"
THIS_DIR = os.path.dirname(os.path.abspath(__file__))


def autolog_info(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Dump the message + the name of this function to the log.
    logging.info(" --> {}".format(message))


def autolog_debug(message):
    """
    Automatically log the current function details."

    :param message: Message to log
    """

    # Get the previous frame in the stack, otherwise it would
    # be this function.
    func = inspect.currentframe().f_back.f_code
    # Dump the message + the name of this function to the log.
    logging.debug(
        "{} in {}:{:d} --> {}".format(
            func.co_name, func.co_filename, func.co_firstlineno, message
        )
    )


def render_sss_env_list(sss_env_list, render_type_str="rst"):
    """
    Use jinja2 to render the environment list.  Pass in all the needed
    data to the sss environment list.

    :param sss_env_list: list of dicts
    :param render_type_str: OPTIONAL.  Default to "rst"
    """

    autolog_info("Rendering env list")

    file_template_str = "autogen-sss-env-list.{}.inc.tmpl".format(render_type_str)
    file_out_str = "{}/{}".format(
        TEMPLATE_OUTPUT_DIR, file_template_str.rsplit(".", 1)[0]
    )

    # Get CWD and create jinja2 environment
    j2_env = jinja2.Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

    file_rendered = j2_env.get_template(
        "{}/{}".format(TEMPLATE_INPUT_DIR, file_template_str)
    ).render(
        generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
        url_env_detail="",
        sss_env_list=sss_env_list,
    )

    with open(file_out_str, "w") as fh:
        fh.write(file_rendered)


def render_sss_env_list_diff_form(sss_env_list, render_type_str="rst"):
    """
    Use jinja2 to render the environment list in the manner of a
    html select.  Pass in all the needed data to the sss environment
    list.

    :param sss_env_list: list of dicts
    :param render_type_str: OPTIONAL.  Default to "rst
    """

    autolog_info("Rendering env list diff form")

    file_template_str = "autogen-sss-env-list-diff-form.{}.inc.tmpl".format(
        render_type_str
    )
    file_out_str = "{}/{}".format(
        TEMPLATE_OUTPUT_DIR, file_template_str.rsplit(".", 1)[0]
    )

    # Get CWD and create jinja2 environment
    j2_env = jinja2.Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

    file_rendered = j2_env.get_template(
        "{}/{}".format(TEMPLATE_INPUT_DIR, file_template_str)
    ).render(
        generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
        url_env_detail="",
        sss_env_list=sss_env_list,
    )

    with open(file_out_str, "w") as fh:
        fh.write(file_rendered)


def determine_core_packages(sss_env_list):
    """
    Find and record the package versions to highlight the core
    packages in each environment

    :param sss_env_list: list of dicts
    """

    # this can change, the rendered output will handle it
    search_core_package_list = ["numpy", "matplotlib", "iris", "cartopy"]

    for env_dict in sss_env_list:
        env_dict["core_packages"] = []

        for package in env_dict["packages"]:
            package_version_short = re.sub("-[0-9].*", "", package["name"])

            if package_version_short in search_core_package_list:
                python_version = re.sub("-(?!.*-).*", "", package["name"])

                # append a tuple for the short version name and the full name
                # so it can be used to link to the package version in the
                # jinja. ie ("cartopy-0.17.0", "cartopy-0.17.0-py36hbb7e04d_1")
                env_dict["core_packages"].append((python_version, package["name"]))


def determine_envs_package_is_in(sss_env_list, package_to_find_dict):
    """
    Search the provided env list for the occurrence of a package

    :param sss_env_list: list of dicts
    :param package_to_find_dict: dict of package
    :return package_is_present_list: list of dicts
    """

    package_is_present_list = []

    for env_dict in sss_env_list:
        for package_dict in env_dict["packages"]:
            # matches on the package name, not the dict {'channel','name'}
            if package_to_find_dict["name"] == package_dict["name"]:
                package_is_present_list.append(env_dict)

    return package_is_present_list


def determine_package_binaries(package_name_str, channel_package_dict):
    """
    Search the provided env list for the occurrence of a package

    :param package_name_str: package name string
    :param channel_package_dict: dict of channels
    :return package_binary_list:
    """

    package_binary_list = []

    for package_binary_str in channel_package_dict:
        if package_name_str + ".tar.bz2" == package_binary_str:
            package_binary_list.append(channel_package_dict[package_binary_str])

    return package_binary_list


def find_channels_package_is_present_in(package_dict, channel_list):
    autolog_debug(
        "Finding what channels a package is in :{}:".format(package_dict["name"])
    )

    channels_package_is_in = []

    for channel in channel_list:
        package_info = {}
        # TODO: tidy up the ".tar.bz2" reference
        if package_dict["name"] + ".tar.bz2" in channel["packages"]:
            autolog_debug(
                "Found :{}: in channel :{}:".format(
                    package_dict["name"], channel["channel"]
                )
            )

            package_info["channel"] = channel["channel"]
            package_info["package"] = channel["packages"][
                package_dict["name"] + ".tar.bz2"
            ]
            channels_package_is_in.append(package_info)

    autolog_debug(
        "Found package :{}: in :{:d}: channels ".format(
            package_dict["name"], len(channels_package_is_in)
        )
    )

    return channels_package_is_in


def render_sss_package_detail(sss_env_list, channel_list, render_type_str="rst"):
    """
    Use jinja2 to render the sss package detail rst.  Pass in all the
    needed data to the sss environment list.

    :param sss_env_list: list of dicts
    :param channel_list: list of dicts
    :param render_type_str: OPTIONAL.  Default to "rst"
    """

    packages_rendered = []
    file_template_str = "autogen-sss-package-detail.{}.tmpl".format(render_type_str)
    count = len(sss_env_list)

    for idx, env_dict in enumerate(sss_env_list):
        autolog_info(
            "({:02d}/{:02d}) Rendering package detail for {} packages"
            " in environment {}".format(
                idx + 1, count, len(env_dict["packages"]), env_dict["env_tag"]
            )
        )

        for package_dict in env_dict["packages"]:
            # already rendered for this package?
            if package_dict["name"] not in packages_rendered:
                packages_rendered.append(package_dict["name"])
                file_out_str = "{}/{}-{}.{}".format(
                    TEMPLATE_OUTPUT_DIR,
                    file_template_str.rsplit(".", 2)[0],
                    package_dict["name"],
                    render_type_str,
                )

                # calculate which envs this package is present in
                sss_env_list_where_package_used = determine_envs_package_is_in(
                    sss_env_list, package_dict
                )

                # get all the details for each channel the package is in,
                # cannot assume just one channel.
                channels_package_is_in = find_channels_package_is_present_in(
                    package_dict, channel_list
                )

                j2_env = jinja2.Environment(
                    loader=FileSystemLoader(THIS_DIR), trim_blocks=True
                )

                file_rendered = j2_env.get_template(
                    "{}/{}".format(TEMPLATE_INPUT_DIR, file_template_str)
                ).render(
                    generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
                    sss_package_info=package_dict,
                    sss_env_present=sss_env_list_where_package_used,
                    sss_channels_package_is_in=channels_package_is_in,
                )

                # write the rendered file
                with open(file_out_str, "w") as fh:
                    fh.write(file_rendered)

    return


def render_sss_package_version_detail(
    sss_env_list, package_versions_dict, package_is_used_in_sss, render_type_str="rst"
):
    """
    Use jinja2 to render the sss package versions.  Pass in all the
    needed data.

    :param sss_env_list: list of dicts
    :param package_versions_dict: list of dicts
    :param package_is_used_in_sss: list of dicts
    :param render_type_str: OPTIONAL.  Default to "rst"
    """

    file_template_str = "autogen-sss-package-versions.{}.tmpl".format(render_type_str)

    autolog_info(
        "Rendering package version detail for {} packages".format(
            len(package_versions_dict)
        )
    )

    for package_name_str in package_versions_dict:
        # if the package is NOT in any envs then skip it...
        if package_name_str in package_is_used_in_sss:
            autolog_debug("package_name_str = {}".format(package_name_str))

            file_out_str = "{}/{}-{}.{}".format(
                TEMPLATE_OUTPUT_DIR,
                file_template_str.rsplit(".", 2)[0],
                package_name_str,
                render_type_str,
            )

            j2_env = jinja2.Environment(
                loader=FileSystemLoader(THIS_DIR), trim_blocks=True
            )

            file_rendered = j2_env.get_template(
                TEMPLATE_INPUT_DIR + "/" + file_template_str
            ).render(
                generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
                sss_package_versions=package_versions_dict[package_name_str],
            )

            # the file should not already exist so no need to check
            with open(file_out_str, "w") as fh:
                fh.write(file_rendered)

    return


def render_sss_all_packages_used(package_is_used_in_sss, render_type_str="rst"):
    """
    Use jinja2 to render the showing a list of all packaged used in the
    SSS.  The rendered output also includes a search box.

    :param package_is_used_in_sss:
    :param render_type_str: OPTIONAL.  Default to "rst"
    """

    autolog_info("Rendering packages used (search)")

    file_template_str = "autogen-sss-packages-used.{}.tmpl".format(render_type_str)
    file_out_str = "{}/{}".format(
        TEMPLATE_OUTPUT_DIR, file_template_str.rsplit(".", 1)[0]
    )

    j2_env = jinja2.Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

    file_rendered = j2_env.get_template(
        TEMPLATE_INPUT_DIR + "/" + file_template_str
    ).render(
        generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
        package_is_used_in_sss=package_is_used_in_sss,
    )

    # write the rendered file
    with open(file_out_str, "w") as fh:
        fh.write(file_rendered)


def render_sss_env_detail(sss_env_list, render_type_str="rst"):
    """
    Use jinja2 to render the environment details.  Pass in all the
    needed data.

    :param sss_env_list: list of dicts
    :param render_type_str: template type, default to "rst"
    """

    autolog_info("Rendering detail for {:d} environments".format(len(sss_env_list)))

    file_template_str = "autogen-sss-env-detail.{}.tmpl".format(render_type_str)

    for env_dict in sss_env_list:
        autolog_debug("Env = {}".format(env_dict["env_tag"]))

        file_out_str = "{}/{}-{}.{}".format(
            TEMPLATE_OUTPUT_DIR,
            file_template_str.rsplit(".", 2)[0],
            env_dict["unique_name"],
            render_type_str,
        )

        j2_env = jinja2.Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)

        # using the list of packages, group by channel and create a
        # dict (channel) of lists (package)

        # copy the list to edit not just point to it
        packages_by_channel_dict = env_dict.copy()
        package_by_channel_dict = {}

        for package_dict in env_dict["packages"]:
            channel_str = package_dict["channel"]

            if channel_str in package_by_channel_dict.keys():
                package_by_channel_dict[channel_str].append(package_dict["name"])
            else:
                package_by_channel_dict[channel_str] = [package_dict["name"]]

        packages_by_channel_dict["packages"] = package_by_channel_dict

        file_rendered = j2_env.get_template(
            "{}/{}".format(TEMPLATE_INPUT_DIR, file_template_str)
        ).render(
            generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
            sss_env_info=[packages_by_channel_dict],
            total_packages=len(env_dict["packages"]),
        )

        with open(file_out_str, "w") as fh:
            fh.write(file_rendered)

    return


def determine_env_python_version(sss_env_list):
    """
    Check the packages in each environments to determine and record the
    python version used.

    :param sss_env_list: list of dicts
    """

    for env_dict in sss_env_list:
        for package_dict in env_dict["packages"]:
            m = re.match("python-\d", package_dict["name"])
            if m:
                python_version_short = re.sub("-(?!.*-).*", "", package_dict["name"])
                env_dict["python_version"] = python_version_short
                env_dict["python_version_full"] = package_dict["name"]

                autolog_debug(
                    "Found Python :{}:, truncated to :{}:".format(
                        package_dict["name"], python_version_short
                    )
                )


def determine_unique_env_names(sss_env_list):
    """
    Cycle through the list of envs and create a unique name based upon
    several values

    :param sss_env_list: list of dicts
    """

    for env_dict in sss_env_list:
        env_dict["unique_name"] = "{}-{}-{}".format(
            env_dict["desc"], env_dict["label"], env_dict["env_tag"]
        )


def load_all_env_manifest(sss_env_list):
    """
    Cycle through the list of envs and load the manifest and add to the
    existing dicts

    :param sss_env_list: list of dicts
    """

    autolog_info("Retrieving manifests for {} environments".format(len(sss_env_list)))

    for env_dict in sss_env_list:
        autolog_debug("loading manifest = {}".format(env_dict["manifest_url"]))

        # line format expected (separator is a tab):
        # "http://www-avd/conda/RedHat.x86_64/linux-64 agg-regrid-0.2.1-py27_0"

        manifest_url_str = query_text_url(env_dict["manifest_url"])
        env_dict["packages"] = []

        for aline in manifest_url_str.splitlines():
            value_list = aline.split("\t")
            env_dict["packages"].append(
                {"channel": value_list[0], "name": value_list[1]}
            )

        # let keep a hold of the original file so we can do a diff
        env_dict["manifest_text"] = manifest_url_str


def render_sss_env_list_diff_html(sss_env_list):
    """
    Cycle through the list of envs and create a matrix of html diffs

    :param sss_env_list: list of dicts
    """

    count = len(sss_env_list)
    autolog_info(
        "Generating the canned env manifest " "diff html for {:d} envs".format(count)
    )

    # jina template to use
    file_template_str = "autogen-sss-env-diff.rst.tmpl"

    for i, env_dict_i in enumerate(sss_env_list):
        autolog_info(
            "({:02d}/{:02d}) Generating diff html "
            "files for {}".format(i + 1, count, env_dict_i["unique_name"])
        )

        for j, env_dict_j in enumerate(sss_env_list):
            autolog_debug(
                "i = {:d} -> {} (len={:d}), "
                "j = {:d} -> {} (len={:d})".format(
                    i,
                    env_dict_i["env_tag"],
                    len(env_dict_i["manifest_text"]),
                    j,
                    env_dict_j["env_tag"],
                    len(env_dict_j["manifest_text"]),
                )
            )

            title_i = "{}-{} ({})".format(
                env_dict_i["desc"], env_dict_i["label"], env_dict_i["released"]
            )
            title_j = "{}-{} ({})".format(
                env_dict_j["desc"], env_dict_j["label"], env_dict_j["released"]
            )

            # diff the manifest as it is
            htmldiff = difflib.HtmlDiff(tabsize=4)
            text_diff_packages = htmldiff.make_file(
                env_dict_i["manifest_text"].split("\n"),
                env_dict_j["manifest_text"].split("\n"),
                title_i,
                title_j,
                context=True,
            )

            # used in the rendered javascript
            text_diff_packages_compressed = text_diff_packages.replace("\n", " ")
            # diff only the packages
            list_packages_only_i = []
            for s in env_dict_i["packages"]:
                list_packages_only_i.append(s["name"])

            list_packages_only_j = []
            for s in env_dict_j["packages"]:
                list_packages_only_j.append(s["name"])

            htmldiff = difflib.HtmlDiff(tabsize=4)
            text_diff_packages_only = htmldiff.make_table(
                list_packages_only_i,
                list_packages_only_j,
                title_i,
                title_j,
                context=True,
            )

            # Example file name:
            # autogen-sss-diff--default-next-env-default-2019_02_27--default-previous-env-default-2018_05_22-1.html

            file_out_str = "{}/autogen-sss-diff--{}--{}.rst".format(
                TEMPLATE_OUTPUT_DIR,
                env_dict_i["unique_name"],
                env_dict_j["unique_name"],
            )

            autolog_debug(
                "Writing diff html = {} (len={:d})".format(
                    file_out_str, len(file_out_str)
                )
            )

            # Get CWD and create jinja2 environment
            j2_env = jinja2.Environment(
                loader=FileSystemLoader(THIS_DIR), trim_blocks=True
            )

            html_str = j2_env.get_template(
                "{}/{}".format(TEMPLATE_INPUT_DIR, file_template_str)
            ).render(
                generated="{:%B %d, %Y %H:%M:%S}".format(datetime.now()),
                text_diff_packages_only=text_diff_packages_only,
                text_diff_packages_compressed=text_diff_packages_compressed,
                text_title="{} vs {}".format(title_i, title_j),
            )

            with open(file_out_str, "w") as fh:
                fh.write(html_str)


def get_channels_list(sss_env_list):
    """
    Cycle through the list of all envs and associated conda channels
    and provide a unique list of channels.

    :param sss_env_list: list of dicts
    :return: list of channels (dicts)
    """

    autolog_info(
        "Finding all the unique channels for {} environments".format(len(sss_env_list))
    )

    channel_list = []

    for env_dict in sss_env_list:
        for package_dict in env_dict["packages"]:
            if package_dict["channel"] not in channel_list:
                autolog_debug(
                    "Found unique conda channel = {}".format(package_dict["channel"])
                )
                channel_list.append(package_dict["channel"])

    autolog_info("Found {:d} channels".format(len(channel_list)))

    return channel_list


def get_api_details(api_key_file):
    """
    """
    autolog_info("Loading API JSON from: {}".format(api_key_file))

    api_host_list = []

    try:
        with open(api_key_file) as f:
            api_host_list = json.load(f)
    except:
        autolog_info("API KEY file not found: {}".format(api_key_file))

    return api_host_list


def load_all_channel_repo_json(channel_list, api_host_list):
    """
    Give all the channel urls, load the reepodata.json of each.

    :param channel_list: List of channel urls strings
    :param api_host_list: List of hosts with user and key credentials
    :return: list of dicts
    """

    count = len(channel_list)
    autolog_info("Retrieving repodata.json from {} channels".format(count))

    channel_list_detail = []

    for idx, channel in enumerate(channel_list):
        autolog_info(
            "({:02d}/{:02d}) Downloading repodata.json "
            "from {}".format(idx + 1, count, channel)
        )

        # check the api hosts list for a match for the channel
        for host_record in api_host_list:
            if host_record["host"] in channel:
                api_user = host_record["user"]
                api_key = host_record["key"]
                break
        else:
            api_user = ""
            api_key = ""

        channel_json = query_json_url(
            "{}/repodata.json".format(channel), api_user, api_key
        )

        channel_list_detail.append(
            {"channel": channel, "packages": channel_json["packages"]}
        )

    return channel_list_detail


def get_package_used_in_sss(package_versions_dict):
    """
    Using a complete list of a packages, use the counter already present
    to return a unique sorted list of packages "used in the sss"

    :param package_versions_dict:
    :return:
    """

    autolog_info(
        "Working out what of the {:d} packages are used "
        "in the environments".format(len(package_versions_dict))
    )

    package_is_used_in_sss = []

    for package_name_str in package_versions_dict:
        for package_version_dict in package_versions_dict[package_name_str]:
            if "sss_envs_count" in package_version_dict:
                package_is_used_in_sss.append(package_name_str)

    return sorted(list(set(package_is_used_in_sss)))


def get_package_version_list(env_list, channel_full_list):
    """
    Cycle through the channels and get the package details (all binaries)

    :param env_list: list of dicts
    :param channel_full_list: list of dicts
    :return: list of dicts
    """

    pkg_ver_dict = {}

    count = len(channel_list)
    autolog_info("Building package version lists for {:d} channels".format(count))

    for idx, channel_dict in enumerate(channel_full_list):
        autolog_info(
            "({:02d}/{:02d}) Building package version list for "
            "channel {}".format(idx + 1, count, channel_dict["channel"])
        )

        for package_binary_str in channel_dict["packages"]:
            pkg_name = channel_dict["packages"][package_binary_str]["name"]

            if pkg_name not in pkg_ver_dict:
                pkg_ver_dict[pkg_name] = [channel_dict["packages"][package_binary_str]]
            else:
                pkg_ver_dict[pkg_name].append(
                    channel_dict["packages"][package_binary_str]
                )

            # add the full version name too
            pkg_ver_dict[pkg_name][-1]["package"] = package_binary_str
            pkg_ver_dict[pkg_name][-1]["package_no_ext"] = package_binary_str.rsplit(
                ".", 2
            )[0]

            # add the ctime if there is a timestamp present
            if "timestamp" in pkg_ver_dict[pkg_name][-1]:
                timestamp_int = pkg_ver_dict[pkg_name][-1]["timestamp"]

                # divide by 1000 as it is currently held in milliseconds.
                ctimestamp = time.ctime(timestamp_int / 1000)
                pkg_ver_dict[pkg_name][-1]["timestamp_str"] = ctimestamp

            # note the channel
            if "channel" in pkg_ver_dict[pkg_name][-1]:
                pkg_ver_dict[pkg_name][-1]["channel"] = (
                    pkg_ver_dict[pkg_name][-1]["channel"] + channel_dict["channel"]
                )
            else:
                pkg_ver_dict[pkg_name][-1]["channel"] = channel_dict["channel"]

            # increment counter if this package version is used in any
            # of the environments
            # TODO: Could instead append to a list of the envs it is used in?
            for env_dict in env_list:
                for sss_package in env_dict["packages"]:
                    # note, this will have duplicate counts if the package is
                    # present in multiple channels.

                    if (
                        sss_package["name"] == package_binary_str.rsplit(".", 2)[0]
                        and sss_package["channel"] == channel_dict["channel"]
                    ):
                        if "sss_envs_count" in pkg_ver_dict[pkg_name][-1]:
                            pkg_ver_dict[pkg_name][-1]["sss_envs_count"] += 1
                        else:
                            pkg_ver_dict[pkg_name][-1]["sss_envs_count"] = 1

    return pkg_ver_dict


def create_env_list_json(sss_env_list):
    """
    Write the environment list to a json file.  Two versions, a simple one and
    a more full one that includes the list of packages.

    :param sss_env_list: list of dicts
    """

    file_out_env_str = "{}/environments.json".format(STATIC_OUTPUT_DIR)
    file_out_env_full_str = "{}/environments-full.json".format(STATIC_OUTPUT_DIR)

    # copy so we can strip content out
    sss_env_list_tmp = copy.deepcopy(sss_env_list)

    # full list, just remove the manifest_text as it is only used for rendering
    # html
    autolog_info(
        "Creating environment list (full) json: {}".format(file_out_env_full_str)
    )

    for env_dict in sss_env_list_tmp:
        if "manifest_text" in env_dict:
            del env_dict["manifest_text"]

    with open(file_out_env_full_str, "w") as fh:
        json.dump(sss_env_list_tmp, fh, indent=4)

    # shorter list.  Strip out the list of packages
    autolog_info("Creating environment list json: {}".format(file_out_env_str))

    for env_dict in sss_env_list_tmp:
        if "packages" in env_dict:
            del env_dict["packages"]

    with open(file_out_env_str, "w") as fh:
        json.dump(sss_env_list_tmp, fh, indent=4)


if __name__ == "__main__":
    """
    Sources all the data into a data structure ready to be used.

    sss_env_list is a LIST of DICTS.  "Packages" is a member of the DICT and
    is a LIST of DICTS also.
    """

    # setup command line arguments
    parser = argparse.ArgumentParser(description="Generate environment browser html.")
    parser.add_argument(
        "--use_cache",
        action="store_true",
        help="Use the cache to avoid hitting urls each run. "
        + "If not present then create it.",
    )
    parser.add_argument(
        "--api_keys",
        type=str,
        help="Point to a file that contains a list of the keys to use "
        + "with the corresponding host and user.  If not set the default "
        + "of "
        + DEFAULT_API_KEYS_FORMAT.format("$HOME")
        + "will be used.  For help "
        + "creating this json file see https://exxgitrepo:8443/projects/SSS/repos/configuration/browse/readthedocs/README.md",
    )

    args = parser.parse_args()

    # check the command line arguments, including the api credentials
    arg_use_cache = args.use_cache
    arg_api_keys = args.api_keys

    if arg_api_keys:
        API_KEYS = arg_api_keys
        autolog_info("API_KEYS set in command line ({})".format(API_KEYS))
    else:
        # default to the $HOME/.artifactory/api_keys.json
        env_home_dir = os.getenv("HOME")
        user_home_api_keys = DEFAULT_API_KEYS_FORMAT.format(env_home_dir)

        if os.path.exists(user_home_api_keys):
            API_KEYS = user_home_api_keys
            autolog_info("API_KEYS found in default location ({})".format(API_KEYS))
        else:
            autolog_info(
                "API_KEYS not found.  Lets continue but any API request will likely fail."
            )

    if arg_use_cache:
        USE_CACHE = True

    cache_present = os.path.exists(FILE_CACHE)

    if USE_CACHE and cache_present:
        autolog_info(
            "USE_CACHE enabled and cache already present ({})".format(FILE_CACHE)
        )

        (
            sss_env_list,
            channel_list,
            channel_full_list,
            package_versions_dict,
            package_is_used_in_sss,
        ) = pickle.load(open(FILE_CACHE, "rb"))
    else:
        autolog_info(
            "USE_CACHE not enabled or cache is " "not present ({})".format(FILE_CACHE)
        )

        # get the SSS rpm list. All data is derived from this
        rpm_list = load_env_rpm_list()

        # derive more details about the envs from bitbucket
        sss_env_list = get_envs_from_rpm_list(rpm_list)

        # load the manifest for all envs and determine some values
        load_all_env_manifest(sss_env_list)
        determine_unique_env_names(sss_env_list)
        determine_env_python_version(sss_env_list)
        determine_core_packages(sss_env_list)

        # load the api data
        api_host_list = get_api_details(API_KEYS)

        # fetch the channel data
        channel_list = get_channels_list(sss_env_list)

        channel_full_list = load_all_channel_repo_json(channel_list, api_host_list)

        package_versions_dict = get_package_version_list(
            sss_env_list, channel_full_list
        )

        package_is_used_in_sss = get_package_used_in_sss(package_versions_dict)

        # useful for development as you can avoid all the above data collection
        # which could be temporarily commented out.
        if USE_CACHE:
            autolog_info(
                "USE_CACHE enabled, so lets create one ({}s)".format(FILE_CACHE)
            )

            pickle.dump(
                [
                    sss_env_list,
                    channel_list,
                    channel_full_list,
                    package_versions_dict,
                    package_is_used_in_sss,
                ],
                open(FILE_CACHE, "wb"),
            )

    # create the json no matter the render format
    create_env_list_json(sss_env_list)

    # render the output wanted
    render_sss_env_list(sss_env_list)
    render_sss_env_detail(sss_env_list)
    render_sss_env_list_diff_form(sss_env_list)
    render_sss_env_list_diff_html(sss_env_list)
    render_sss_package_detail(sss_env_list, channel_full_list)
    render_sss_package_version_detail(
        sss_env_list, package_versions_dict, package_is_used_in_sss
    )
    render_sss_all_packages_used(package_is_used_in_sss)
