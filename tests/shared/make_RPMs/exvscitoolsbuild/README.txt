See https://exxreldocs:8099/display/AVD/How+to+test+conda-rpms+in+a+CentOS+Docker+container

- sudo docker container run -it --rm bjlittle/centos-rpm bash
- create the file /etc/yum.repos.d/scitools.repo
- yum repolist
- yum --disablerepo="*" --enablerepo="scitools" list available
- yum -y --disablerepo="*" --enablerepo="scitools" install SciTools-env-default-label-current-5-0.x86_64 

References:
- https://exxreldocs:8099/display/AVD/Docker+Cheat+Sheet
- https://access.redhat.com/sites/default/files/attachments/rh_yum_cheatsheet_1214_jcs_print-1.pdf
- https://www.digitalocean.com/community/tutorials/how-to-set-up-and-use-yum-repositories-on-a-centos-6-vps
- https://www.thegeekstuff.com/2011/08/yum-command-examples/
- https://github.com/projectatomic/docker-image-examples/blob/master/centos-ruby-selfcontained/Dockerfile
